package com.learning.spring.services.serviceImpl

import com.learning.spring.models.Commune
import com.learning.spring.repositories.CommuneRepository
import com.learning.spring.services.CommuneService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class CommuneServiceImpl: CommuneService {

    @Autowired
    lateinit var communeRepository: CommuneRepository

    override fun getCommuneByDistrictId(distId: Long): List<Commune> {
        return communeRepository.findByDistrictIdAndStatusTrue(distId)
    }

    override fun findAllList(q: String?, page: Int, size: Int): Page<Commune>? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findById(id: Long): Commune? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun addNew(t: Commune): Commune? {
        return communeRepository.save(t)
    }

    override fun updateObj(id: Long, t: Commune): Commune? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findAll(): List<Commune>? {
        return communeRepository.findAll()
    }
}