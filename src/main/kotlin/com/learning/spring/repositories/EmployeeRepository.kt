package com.learning.spring.repositories

import com.learning.spring.models.Employee
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository

@Repository
interface EmployeeRepository: JpaRepository<Employee, Long>, JpaSpecificationExecutor<Employee> {
    fun findAllByStatusTrueOrderByIdDesc(): List<Employee>?
}