package com.learning.spring.repositories

import com.learning.spring.models.PaymentTerm
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository

@Repository
interface PaymentTermRepository:JpaRepository<PaymentTerm,Long>,JpaSpecificationExecutor<PaymentTerm>{
    fun findAllByStatusTrueOrderByIdDesc():List<PaymentTerm>?
}