package com.learning.spring.services

import com.learning.spring.bases.service.BaseService
import com.learning.spring.models.ItemPrice

interface ItemPriceService: BaseService<ItemPrice> {
}