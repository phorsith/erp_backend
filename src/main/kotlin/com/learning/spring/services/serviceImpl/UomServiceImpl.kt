package com.learning.spring.services.serviceImpl

import com.learning.spring.models.Uom
import com.learning.spring.repositories.UomRepository
import com.learning.spring.services.UomService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class UomServiceImpl: UomService {

    @Autowired
    lateinit var uomRepository: UomRepository

    override fun findAllList(q: String?, page: Int, size: Int): Page<Uom>? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findById(id: Long): Uom? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun addNew(t: Uom): Uom? {
        return uomRepository.save(t)
    }

    override fun updateObj(id: Long, t: Uom): Uom? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findAll(): List<Uom>? {
        return uomRepository.findAllByStatusTrueOrderByIdDesc()
    }
}