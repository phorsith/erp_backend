package com.learning.spring.services.serviceImpl

import com.learning.spring.models.SupplierGroup
import com.learning.spring.repositories.SupplierGroupRepository
import com.learning.spring.services.SupplierGroupService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class SupplierGroupServiceImpl: SupplierGroupService {

    @Autowired
    lateinit var supplierGroupRepository: SupplierGroupRepository

    override fun findAllList(q: String?, page: Int, size: Int): Page<SupplierGroup>? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findById(id: Long): SupplierGroup? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun addNew(t: SupplierGroup): SupplierGroup? {
        return supplierGroupRepository.save(t)
    }

    override fun updateObj(id: Long, t: SupplierGroup): SupplierGroup? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findAll(): List<SupplierGroup>? {
        return supplierGroupRepository.findAllByStatusTrueOrderByIdDesc()
    }
}