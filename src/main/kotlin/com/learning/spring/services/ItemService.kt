package com.learning.spring.services

import com.learning.spring.bases.service.BaseService
import com.learning.spring.models.Item
import com.learning.spring.models.request.ItemRequest
import org.springframework.web.multipart.MultipartFile

interface ItemService: BaseService<Item> {
    fun addNewItem(item: ItemRequest): Item?
    fun uploadItemImage(id: Long, image: MultipartFile): Item?
}