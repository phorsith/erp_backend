package com.learning.spring.models.request

import com.learning.spring.models.*
class ItemRequest (

        var id: Long ? = null,
        var itemName: String? = "",
        var itemCode:String? = "",
        var openStock: String? = "",
        var standardBuyingPrice: Int? = null,
        var standardSellingPrice: Int? = null,
        var description: String? = "",
        var imagePart: String? = "",
        var image: String? = "",
        var hasBatchNo: Boolean? = true,
        var hasSerialNo: Boolean? = true,
        var serialNo: String? = "",
        var maxDiscount: Int? = null,
        var cost: Int? = null,
        var saleUnitOfMeasure: Int? = null,
        var manufacturePartNumber: Int? = null,
        var disable: Boolean? = true,
        var absoluteValue: Boolean? = true,
        var fixedAsset: Boolean? = true,
        var maintainStock: Boolean? = true,
        var isSaleItem: Boolean? = true,
        var barcode: String?= "",
        var itemGroup: ItemGroup? = null,
        var valuateMethod: ValuateMethod? = null,
        var uom: Uom? = null,
        var itemBranch: ItemBranch? = null,
        var supplier: Supplier? = null,
        var itemVariantUom: MutableList<ItemVariantUom>? = null
)