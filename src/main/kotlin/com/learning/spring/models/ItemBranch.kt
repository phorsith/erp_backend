package com.learning.spring.models

import com.learning.spring.bases.model.BaseEntity
import javax.persistence.*

@Entity
@Table(name="item_branch")
class ItemBranch(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long ? = null,
        @Column(name="item_branch")
        var branchName: String? = "",
        var description: String? = ""
): BaseEntity()