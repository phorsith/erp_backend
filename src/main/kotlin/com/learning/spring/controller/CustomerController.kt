package com.learning.spring.controller

import com.learning.spring.models.Customer
import com.learning.spring.models.CustomerGroup
import com.learning.spring.models.CustomerType
import com.learning.spring.services.CustomerDropdownService
import com.learning.spring.services.CustomerGroupService
import com.learning.spring.services.CustomerService
import com.learning.spring.services.CustomerTypeService
import com.learning.spring.utils.ResponseObjectMap
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/customer")
class CustomerController {
    @Autowired
    lateinit var customerService: CustomerService

    @Autowired
    lateinit var customerGroupService: CustomerGroupService

    @Autowired
    lateinit var customerTypeService: CustomerTypeService

    @Autowired
    lateinit var customerDropdownService: CustomerDropdownService

    val response = ResponseObjectMap()

    @PostMapping()
    fun addCustomer(@RequestBody customer: Customer): MutableMap<String,Any> =
        response.responseObject(customerService.addNew(customer))

    @GetMapping("/all")
    fun getAllCustomer():MutableMap<String,Any> =
        response.responseObject(customerService.findAll())

    @PostMapping("/group")
    fun addCustomerGroup(@RequestBody customerGroup: CustomerGroup): MutableMap<String,Any> =
        response.responseObject(customerGroupService.addNew(customerGroup))

    @GetMapping("/group/all")
    fun getAllCustomerGroup():MutableMap<String,Any> =
        response.responseObject(customerGroupService.findAll())

    @PostMapping("/type")
    fun addCustomerType(@RequestBody customerType: CustomerType): MutableMap<String,Any> =
        response.responseObject(customerTypeService.addNew(customerType))

    @GetMapping("/type/all")
    fun getAllCustomerType():MutableMap<String,Any> =
        response.responseObject(customerTypeService.findAll())

    @GetMapping("/dropdown")
    fun getCustomerDropDownList(): MutableMap<String, Any> ?=
        response.responseObject(customerDropdownService.getCustomerDropdownList())

    @GetMapping("/list")
    fun findAllCustomerCriteria(@RequestParam(required = false) query: String?, size: Int, page: Int) : MutableMap<String, Any> {
        val itemPage = customerService.findAllList(query, page, size)
        val item = itemPage?.content
        val totalElements = itemPage?.totalElements
        return response.responseObject(item, totalElements!!)
    }

    @PutMapping("update/{id}")
    fun updateCustomer(@PathVariable id: Long, @RequestBody customer: Customer): MutableMap<String, Any> =
        response.responseObject(customerService.updateObj(id, customer))

    //@PutMapping()
    // delete
}