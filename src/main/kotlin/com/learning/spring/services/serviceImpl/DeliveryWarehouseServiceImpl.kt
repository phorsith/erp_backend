package com.learning.spring.services.serviceImpl

import com.learning.spring.models.DeliveryWarehouse
import com.learning.spring.repositories.DeliveryWarehouseRepository
import com.learning.spring.services.DeliveryWarehouseService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class DeliveryWarehouseServiceImpl:DeliveryWarehouseService {
    @Autowired
    lateinit var deliveryWarehouseRepository: DeliveryWarehouseRepository
    override fun findAllList(q: String?, page: Int, size: Int): Page<DeliveryWarehouse>? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findById(id: Long): DeliveryWarehouse? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun addNew(t: DeliveryWarehouse): DeliveryWarehouse? {
        return deliveryWarehouseRepository.save(t)
    }

    override fun updateObj(id: Long, t: DeliveryWarehouse): DeliveryWarehouse? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findAll(): List<DeliveryWarehouse>? {
        return deliveryWarehouseRepository.findAll()
    }
}