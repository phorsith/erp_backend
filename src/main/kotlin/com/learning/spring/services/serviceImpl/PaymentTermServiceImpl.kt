package com.learning.spring.services.serviceImpl

import com.learning.spring.models.PaymentTerm
import com.learning.spring.repositories.PaymentTermRepository
import com.learning.spring.services.PaymentTermService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class PaymentTermServiceImpl:PaymentTermService {

    @Autowired
    lateinit var paymentTermRepository: PaymentTermRepository
    override fun findAllList(q: String?, page: Int, size: Int): Page<PaymentTerm>? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findById(id: Long): PaymentTerm? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun addNew(t: PaymentTerm): PaymentTerm? {
        return paymentTermRepository.save(t)
    }

    override fun updateObj(id: Long, t: PaymentTerm): PaymentTerm? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findAll(): List<PaymentTerm>? {
      return paymentTermRepository.findAllByStatusTrueOrderByIdDesc()
    }
}