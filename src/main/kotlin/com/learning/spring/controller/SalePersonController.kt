package com.learning.spring.controller

import com.learning.spring.models.SalePerson
import com.learning.spring.services.SalePersonService
import com.learning.spring.utils.ResponseObjectMap
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/sale")
class SalePersonController {
    @Autowired
    lateinit var saleService: SalePersonService
    val response = ResponseObjectMap()

    @PostMapping()
    fun addNew(@RequestBody salePerson: SalePerson): MutableMap<String,Any>
            = response.responseObject(saleService.addNew(salePerson))

    @GetMapping("/all")
    fun getAllSale(): MutableMap<String,Any> = response.responseObject(saleService.findAll())
}