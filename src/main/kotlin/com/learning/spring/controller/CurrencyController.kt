package com.learning.spring.controller

import com.learning.spring.models.Currency
import com.learning.spring.services.CurrencyService
import com.learning.spring.utils.ResponseObjectMap
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/currency")
class CurrencyController {
    @Autowired
    lateinit var currencyService: CurrencyService
    val response = ResponseObjectMap()

    @PostMapping()
    fun addCurrency(@RequestBody currency: Currency): MutableMap<String,Any> =
            response.responseObject(currencyService.addNew(currency))

    @GetMapping("/all")
    fun getAllCurrency(): MutableMap<String,Any> = response.responseObject(currencyService.findAll())

}
