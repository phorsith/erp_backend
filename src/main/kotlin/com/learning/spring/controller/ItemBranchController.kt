package com.learning.spring.controller

import com.learning.spring.models.ItemBranch
import com.learning.spring.services.ItemBranchService
import com.learning.spring.utils.ResponseObjectMap
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/item_branch")
class ItemBranchController {
    @Autowired
    lateinit var itemBranchService: ItemBranchService
    val response = ResponseObjectMap()

    @PostMapping()
    fun addItemBranch(@RequestBody itemBranch: ItemBranch): MutableMap<String,Any>
            = response.responseObject(itemBranchService.addNew(itemBranch))

    @GetMapping("/all")
    fun getAllItemBranch(): MutableMap<String,Any> = response.responseObject(itemBranchService.findAll())

}