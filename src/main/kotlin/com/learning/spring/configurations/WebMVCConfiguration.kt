package com.learning.spring.configurations

import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.*

/***
 * @author PHOR Sith
 * Date: 2020/07/10
 * @Back End Developer
 */

@Configuration
@EnableWebMvc
class WebMVCConfiguration : WebMvcConfigurer {
    val resourcesLocation = arrayOf("classpath:/META-INF/resources/", "classpath:/resources/", "classpath:/static/", "classpath:/public/")
    /**
     *
     * Configure cross origin
     */
    override fun addCorsMappings(registry: CorsRegistry) {
        registry.addMapping("/**")
                .allowedOrigins("http://192.168.2.57:8000",
                        "http://192.168.2.54:8000",
                        "http://192.168.2.250:8000",
                        "http://localhost:9876")
                .allowedMethods("GET", "POST", "PUT", "DELETE", "OPTIONS")
    }

    /**
     *
     * Configure resources image and swagger
     */
    override fun addResourceHandlers(registry: ResourceHandlerRegistry) {
        registry.addResourceHandler("/**")
                .addResourceLocations(*resourcesLocation)
    }

    override fun addViewControllers(registry: ViewControllerRegistry) {
        registry.addRedirectViewController("swagger", "/swagger")
    }

}