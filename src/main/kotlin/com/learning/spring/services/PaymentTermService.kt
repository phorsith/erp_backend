package com.learning.spring.services

import com.learning.spring.bases.service.BaseService
import com.learning.spring.models.PaymentTerm

interface PaymentTermService:BaseService<PaymentTerm>{
}