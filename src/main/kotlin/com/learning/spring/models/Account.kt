package com.learning.spring.models

import com.learning.spring.bases.model.BaseEntity
import javax.persistence.*

@Entity
@Table(name="account")
class Account(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long ? = null,
        @Column(name="account_name")
        var accountName: String? = "",
        var description: String? = "",

        @OneToOne
        @JoinColumn(name = "accounttype_id", nullable = false)
        var accountType: AccountType? = null

): BaseEntity()