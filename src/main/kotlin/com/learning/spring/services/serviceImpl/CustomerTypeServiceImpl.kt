package com.learning.spring.services.serviceImpl

import com.learning.spring.models.CustomerType
import com.learning.spring.repositories.CustomerTypeRepository
import com.learning.spring.services.CustomerTypeService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class CustomerTypeServiceImpl: CustomerTypeService {

    @Autowired
    lateinit var customerTypeRepository: CustomerTypeRepository

    override fun findAllList(q: String?, page: Int, size: Int): Page<CustomerType>? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findById(id: Long): CustomerType? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun addNew(t: CustomerType): CustomerType? {
        return customerTypeRepository.save(t)
    }

    override fun updateObj(id: Long, t: CustomerType): CustomerType? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findAll(): List<CustomerType>? {
        return customerTypeRepository.findAll()
    }
}