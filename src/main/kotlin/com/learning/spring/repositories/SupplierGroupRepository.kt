package com.learning.spring.repositories

import com.learning.spring.models.SupplierGroup
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository

@Repository
interface SupplierGroupRepository: JpaRepository<SupplierGroup, Long>,JpaSpecificationExecutor<SupplierGroup> {
    fun findAllByStatusTrueOrderByIdDesc(): List<SupplierGroup>?
}