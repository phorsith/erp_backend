package com.learning.spring.controller

import com.learning.spring.models.Address
import com.learning.spring.services.AddressService
import com.learning.spring.utils.ResponseObjectMap
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/address")
class AddressController {
    @Autowired
    lateinit var addressService: AddressService
    val response = ResponseObjectMap()

    @PostMapping()
    fun addAddress(@RequestBody address: Address): MutableMap<String,Any> =
        response.responseObject(addressService.addNew(address))

    @GetMapping("/all")
    fun getAllAddress(): MutableMap<String,Any> =
        response.responseObject(addressService.findAll())

}