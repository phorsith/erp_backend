package com.learning.spring.utils

/***
 * @author PHOR Sith
 * Date: 2020/07/10
 * @Back End Developer
 */

object SwaggerConstant {
    const val TITLE = "Learning Spring Project"
    const val DESCRIPTION = "Learning Spring Backend API"
    const val VERSION = "1.0"
    const val TERM_OF_SERVICE_URL = "Term of service"
    const val CONTACT_NAME = "Learning Spring Team"
    const val CONTACT_URL = "http://www.learningspring.com/contact-us/"
    const val CONTACT_EMAIL = "phosith22@gmail.com"
    const val CONTACT_VERSION = "Apache License Version 2.0"
    const val CONTACT_URL_VERSION = "https://www.apache.org/licenses/LICENSE-2.0"
}