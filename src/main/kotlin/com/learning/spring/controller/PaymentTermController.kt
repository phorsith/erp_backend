package com.learning.spring.controller

import com.learning.spring.models.PaymentTerm
import com.learning.spring.services.PaymentTermService
import com.learning.spring.utils.ResponseObjectMap
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/payment")
class PaymentTermController {
    @Autowired
    lateinit var paymentTermService: PaymentTermService
    val response = ResponseObjectMap()

    @PostMapping()
    fun addPaymentTerm(@RequestBody paymentTerm: PaymentTerm): MutableMap<String,Any> =
            response.responseObject(paymentTermService.addNew(paymentTerm))

    @GetMapping("/all")
    fun getAllPaymentTerm(): MutableMap<String,Any> = response.responseObject(paymentTermService.findAll())
}