package com.learning.spring.bases.model


import com.fasterxml.jackson.annotation.JsonIgnore
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.util.*
import javax.persistence.*

/***
 * @author PHOR Sith
 * Date: 2020/07/10
 * @Back End Developer
 */

@MappedSuperclass
@EntityListeners(AuditingEntityListener::class)
open class BaseEntity {
    @Column(name = "status", nullable = false,columnDefinition = "boolean default true")
    var status : Boolean = true

    @Column(name = "version")
    @Version
    @JsonIgnore
    var version: Int ?= 0
    @JsonIgnore
    @Column(name = "date_created")
    var created: Date? = null
    @JsonIgnore
    @Column(name = "last_updated")
    var updated: Date? = null
    @JsonIgnore
    @Column(name = "created_by_id")
    val createById: Int? = null
    @JsonIgnore
    @Column(name = "updated_by_id")
    val updatedById: Int? = null

    /**
     *
     */
    @PrePersist
    protected fun onCreate() {
        created = Date()
    }

    /**
     *
     */
    @PreUpdate
    protected fun onUpdate() {
        updated = Date()
    }
}