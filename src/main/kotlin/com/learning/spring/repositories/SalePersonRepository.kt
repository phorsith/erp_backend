package com.learning.spring.repositories

import com.learning.spring.models.SalePerson
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository

@Repository
interface SalePersonRepository:JpaRepository<SalePerson,Long>,JpaSpecificationExecutor<SalePerson> {
}