package com.learning.spring.controller

import com.learning.spring.models.DeliveryWarehouse
import com.learning.spring.services.DeliveryWarehouseService
import com.learning.spring.utils.ResponseObjectMap
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/warehouse")
class DeliveryWarehouseController {
    @Autowired
    lateinit var deliveryWarehouseService: DeliveryWarehouseService
    val response = ResponseObjectMap()

    @PostMapping()
    fun addWarehouse(@RequestBody deliveryWarehouse: DeliveryWarehouse): MutableMap<String,Any> =
            response.responseObject(deliveryWarehouseService.addNew(deliveryWarehouse))

    @GetMapping("/all")
    fun getAllWarehouse(): MutableMap<String,Any> = response.responseObject(deliveryWarehouseService.findAll())

}