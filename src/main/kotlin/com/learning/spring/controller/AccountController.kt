package com.learning.spring.controller

import com.learning.spring.models.Account
import com.learning.spring.models.Item
import com.learning.spring.services.AccountService
import com.learning.spring.services.ItemService
import com.learning.spring.utils.ResponseObjectMap
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/account")
class AccountController {
    @Autowired
    lateinit var accountService: AccountService

    val response = ResponseObjectMap()

    @PostMapping()
    fun addAccount(@RequestBody account: Account): MutableMap<String,Any> =
        response.responseObject(accountService.addNew(account))

    @GetMapping("/all")
    fun getAllAccount(): MutableMap<String,Any> =
        response.responseObject(accountService.findAll())

    @GetMapping("/list")
    fun findAllAccountCriteria(@RequestParam(required = false) query: String?, size: Int, page: Int) : MutableMap<String, Any> {
        val itemPage = accountService.findAllList(query, page, size)
        val item = itemPage?.content
        val totalElements = itemPage?.totalElements
        return response.responseObject(item, totalElements!!)
    }

    @PutMapping("/update/{id}")
    fun updateAccount(@PathVariable id: Long, @RequestBody account: Account): MutableMap<String,Any> =
            response.responseObject(accountService.updateObj(id, account))

}