package com.learning.spring.services.serviceImpl

import com.learning.spring.models.Role
import com.learning.spring.repositories.RoleRepository
import com.learning.spring.services.RoleService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class RoleServiceImpl: RoleService {

    @Autowired
    lateinit var roleRepository: RoleRepository

    override fun findAllList(q: String?, page: Int, size: Int): Page<Role>? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findById(id: Long): Role? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun addNew(t: Role): Role? {
        return roleRepository.save(t)
    }

    override fun updateObj(id: Long, t: Role): Role? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findAll(): List<Role>? {
       return roleRepository.findAll()
    }
}