package com.learning.spring.services.serviceImpl

import com.learning.spring.models.Address
import com.learning.spring.repositories.AddressRepository
import com.learning.spring.services.AddressService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class AddressServiceImpl: AddressService {

    @Autowired
    lateinit var addressRepository: AddressRepository
    override fun findAllList(q: String?, page: Int, size: Int): Page<Address>? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findById(id: Long): Address? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun addNew(t: Address): Address? {
        return addressRepository.save(t)
    }

    override fun updateObj(id: Long, t: Address): Address? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findAll(): List<Address>? {
        return addressRepository.findAllByStatusTrueOrderByIdDesc()
    }
}