package com.learning.spring.controller

import com.learning.spring.models.Item
import com.learning.spring.models.request.ItemRequest
import com.learning.spring.services.ItemDropdownService
import com.learning.spring.services.ItemService
import com.learning.spring.utils.FileUpload
import com.learning.spring.utils.ResponseObjectMap
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import javax.servlet.http.HttpServletRequest

@RestController
@RequestMapping("api/item")
class ItemController {
    @Autowired
    lateinit var itemService: ItemService
    @Autowired
    lateinit var itemDropdownService: ItemDropdownService

    val response = ResponseObjectMap()

    @PostMapping()
    fun addItem(@RequestBody item: ItemRequest): MutableMap<String,Any>
        = response.responseObject(itemService.addNewItem(item))

    @GetMapping("/all")
    fun getAllItem(): MutableMap<String,Any> =
            response.responseObject(itemService.findAll())

    @GetMapping("/item-dropdown")
    fun getItemDropdownList(): MutableMap<String, Any> =
            response.responseObject(itemDropdownService.getItemDropdownList())

    @PostMapping("/item-image/{id}")
    fun uploadUserProfile(@PathVariable id: Long, image: MultipartFile?):Map<String,Any>{
        return response.responseObject(itemService.uploadItemImage(id,image!!))
    }

    // get user profile
    @Value("\${item.image}")
    var image: String? = null
    @GetMapping("/images/{fileName:.+}")
    fun getUserProfile(@PathVariable("fileName") imagesName: String, request: HttpServletRequest): ResponseEntity<*>? {
        println("getImage :$imagesName")
        return try {
            FileUpload.loadFile(imagesName, image!!, request)
        }
        catch (ex:Exception) {
            null
        }
    }

    @GetMapping("/list")
    fun findAllItemCriteria(@RequestParam(required = false) query: String?, size: Int, page: Int) : MutableMap<String, Any> {
        val itemPage = itemService.findAllList(query, page, size)
        val item = itemPage?.content
        val totalElements = itemPage?.totalElements
        return response.responseObject(item, totalElements!!)
    }

    @PutMapping("/update/{id}")
    fun updateItem(@PathVariable id: Long, @RequestBody item: Item): MutableMap<String,Any> =
        response.responseObject(itemService.updateObj(id, item))

}