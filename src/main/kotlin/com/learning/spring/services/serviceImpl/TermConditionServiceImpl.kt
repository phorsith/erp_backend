package com.learning.spring.services.serviceImpl

import com.learning.spring.models.TermCondition
import com.learning.spring.repositories.TermConditionRepository
import com.learning.spring.services.TermConditionService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class TermConditionServiceImpl: TermConditionService {

    @Autowired
    lateinit var termConditionRepository: TermConditionRepository

    override fun findAllList(q: String?, page: Int, size: Int): Page<TermCondition>? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findById(id: Long): TermCondition? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun addNew(t: TermCondition): TermCondition? {
       return termConditionRepository.save(t)
    }

    override fun updateObj(id: Long, t: TermCondition): TermCondition? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findAll(): List<TermCondition>? {
        return termConditionRepository.findAllByStatusTrueOrderByIdDesc()
    }
}