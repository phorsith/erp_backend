package com.learning.spring.services.serviceImpl

import com.learning.spring.models.District
import com.learning.spring.repositories.DistrictRepository
import com.learning.spring.services.DistrictService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class DistrictServiceImpl: DistrictService {
    @Autowired
    lateinit var districtRepository: DistrictRepository

    override fun getDistrictByProvinceId(provId: Long): List<District> {
        return districtRepository.findByProvinceIdAndStatusTrue(provId)
    }

    override fun findAllList(q: String?, page: Int, size: Int): Page<District>? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findById(id: Long): District? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun addNew(t: District): District? {
        return districtRepository.save(t)
    }

    override fun updateObj(id: Long, t: District): District? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findAll(): List<District>? {
        return districtRepository.findAll()
    }

}