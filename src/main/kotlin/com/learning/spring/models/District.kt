package com.learning.spring.models


import com.fasterxml.jackson.annotation.JsonIgnore
import com.learning.spring.bases.model.BaseEntity
import javax.persistence.*

@Entity
@Table(name="district")
class District(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long ? = null,
        var district: String?="",
        @Column(name = "district_kh")
        var districtKhmer: String?="",
        var distid: String? = "",
       // @JsonIgnore
        @ManyToOne
        @JoinColumn(name="province_id", nullable=false)
        var province: Province ?= null,

                @JsonIgnore
         @OneToMany(mappedBy = "district", cascade = [CascadeType.ALL])
         var commune: MutableList<Commune> ?= null


): BaseEntity()