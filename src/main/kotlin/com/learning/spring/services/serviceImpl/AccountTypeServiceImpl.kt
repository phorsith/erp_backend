package com.learning.spring.services.serviceImpl

import com.learning.spring.models.AccountType
import com.learning.spring.repositories.AccountTypeRepository
import com.learning.spring.services.AccountTypeService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class AccountTypeServiceImpl: AccountTypeService {
    @Autowired
    lateinit var accountTypeRepository: AccountTypeRepository

    override fun findAllList(q: String?, page: Int, size: Int): Page<AccountType>? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findById(id: Long): AccountType? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun addNew(t: AccountType): AccountType? {
        return accountTypeRepository.save(t)
    }

    override fun updateObj(id: Long, t: AccountType): AccountType? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findAll(): List<AccountType>? {
        return accountTypeRepository.findAllByStatusTrueOrderByIdDesc()
    }
}