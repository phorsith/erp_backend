package com.learning.spring.models

import com.learning.spring.bases.model.BaseEntity
import javax.persistence.*

@Entity
@Table(name="supplier")
class Supplier(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long ? = null,
        @Column(name="supplier_name")
        var supplierName: String? = ""
): BaseEntity()