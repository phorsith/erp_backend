package com.learning.spring.services.serviceImpl

import com.learning.spring.models.Item
import com.learning.spring.models.ItemVariantUom
import com.learning.spring.models.request.ItemRequest
import com.learning.spring.repositories.ItemRepository
import com.learning.spring.repositories.ItemVariantUomRepository
import com.learning.spring.services.ItemService
import com.learning.spring.utils.FileUpload
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import javax.persistence.criteria.Predicate

@Service
class ItemServiceImpl: ItemService {

    @Autowired
    lateinit var itemRepository: ItemRepository
    @Autowired
    lateinit var itemVariantUomRepository: ItemVariantUomRepository

    @Value("\${item.image}")
    var itemImage: String? = null

    override fun addNewItem(item: ItemRequest): Item? {
        val saveItem = Item(
               itemName = item.itemName,
               itemCode = item.itemCode,
               openStock = item.openStock,
               standardBuyingPrice = item.standardBuyingPrice,
               standardSellingPrice = item.standardSellingPrice,
               description = item.description,
               hasBatchNo = item.hasBatchNo,
               hasSerialNo = item.hasSerialNo,
               serialNo = item.serialNo,
               maxDiscount = item.maxDiscount,
               cost = item.cost,
               saleUnitOfMeasure = item.saleUnitOfMeasure,
               manufacturePartNumber = item.manufacturePartNumber,
               disable = item.disable,
               absoluteValue = item.absoluteValue,
               fixedAsset = item.fixedAsset,
               maintainStock = item.maintainStock,
               isSaleItem = item.isSaleItem,
               barcode = item.barcode,
               itemGroup = item.itemGroup,
               valuateMethod = item.valuateMethod,
               uom = item.uom,
               itemBranch = item.itemBranch,
               supplier = item.supplier

        )
        val newItem = itemRepository.save(saveItem)
        item.itemVariantUom?.forEach {
            val newItemVariantUom = ItemVariantUom(
                    conversionFactor = it.conversionFactor,
                    uom = it.uom,
                    item = newItem
            )
            itemVariantUomRepository.save(newItemVariantUom)
        }
        return newItem
    }

    override fun uploadItemImage(id: Long, image: MultipartFile): Item? {

        val originalFileName = FileUpload.storeImage(image, itemImage)
        val item = itemRepository.findByIdAndStatusTrue(id)
        item!!.imagePart = "/item/images/$originalFileName"
        println("user part ${item.imagePart}")
        item.image = originalFileName
        println("user part ${item.image}")

        return itemRepository.save(item)
    }

    override fun findAllList(q: String?, page: Int, size: Int): Page<Item>? {
        return itemRepository.findAll({root, query, cb ->
            val predicates = ArrayList<Predicate>()
            if (q != null) {
                val name = cb.like(cb.upper(root.get("name")), "%${q.toUpperCase()}%")
                val email = cb.like(cb.upper(root.get("email")), "%${q.toUpperCase()}%")
                predicates.add(cb.or( name, email))
            }
            predicates.add(cb.equal(root.get<String>("status"), true))
            query.orderBy(cb.desc(root.get<String>("id")))
            cb.and(*predicates.toTypedArray())
        }, PageRequest.of(page, size))
    }

    override fun findById(id: Long): Item? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun addNew(t: Item): Item? {

        return itemRepository.save(t)
    }

    override fun updateObj(id: Long, t: Item): Item? {
        val item = itemRepository.findByIdAndStatusTrue(id)
        if (!t.status) item!!.status = false
            item?.itemName = t.itemName
            item?.itemCode = t.itemCode
            item?.openStock = t.openStock
            item?.standardBuyingPrice = t.standardBuyingPrice
            item?.standardSellingPrice = t.standardSellingPrice
            item?.description = t.description
            item?.hasBatchNo = t.hasBatchNo
            item?.hasSerialNo = t.hasSerialNo
            item?.serialNo = t.serialNo
            item?.maxDiscount = t.maxDiscount
            item?.cost = t.cost
            item?.saleUnitOfMeasure = t.saleUnitOfMeasure
            item?.manufacturePartNumber = t.manufacturePartNumber
            item?.disable = t.disable
            item?.absoluteValue = t.absoluteValue
            item?.fixedAsset = t.fixedAsset
            item?.maintainStock = t.maintainStock
            item?.isSaleItem = t.isSaleItem
            item?.barcode = t.barcode
            item?.itemGroup = t.itemGroup
            item?.valuateMethod = t.valuateMethod
            item?.uom = t.uom
            item?.itemBranch = t.itemBranch
            item?.supplier = t.supplier

        return itemRepository.save(item!!)
    }

    override fun findAll(): List<Item>? {
        return itemRepository.findAllByStatusTrueOrderByIdDesc()
    }
}