package com.learning.spring.services

import com.learning.spring.models.ItemDropdown

interface ItemDropdownService {
    //fun getCustomerDropdownList(): CustomerDropdown?
    fun getItemDropdownList(): ItemDropdown?
}