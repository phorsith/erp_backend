package com.learning.spring.controller

import com.learning.spring.models.Users
import com.learning.spring.services.UsersService
import com.learning.spring.utils.FileUpload
import com.learning.spring.utils.ResponseObjectMap
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import javax.servlet.http.HttpServletRequest

@RestController
@RequestMapping("api/user")
class UsersController {
    @Autowired
    lateinit var usersService: UsersService

    @Autowired
    lateinit var passwordEncoder: PasswordEncoder

    val response = ResponseObjectMap()

    @PostMapping()
    fun addUsers(@RequestBody users: Users):MutableMap<String,Any> {
        users.password = passwordEncoder.encode(users.password)
       return response.responseObject(usersService.addNew(users))
    }

    @GetMapping("/all")
    fun getAllUsers():MutableMap<String,Any> = response.responseObject(usersService.findAll())

    @GetMapping("/{id}")
    fun getUserById(@PathVariable id: Long):MutableMap<String,Any> =
            response.responseObject(usersService.findById(id))

    @GetMapping("/list")
    fun findUsersList(@RequestParam(value = "query", required = false) query: String?,
                      @RequestParam(value = "page", defaultValue = "0") page: Int,
                      @RequestParam(value = "size", defaultValue = "10") size: Int): MutableMap<String, Any> {
        val userPage = usersService.findAllList(query, page, size)
        val user = userPage?.content
        val totalElements = userPage?.totalElements
        return response.responseObject(user, totalElements!!)
    }

    @PutMapping("/{id}")
    fun updateUser(@PathVariable id: Long, @RequestBody user: Users): MutableMap<String, Any> =
            response.responseObject(usersService.updateObj(id,user))

    @PostMapping("/profile/{id}")
    fun uploadUserProfile(@PathVariable id: Long, image:MultipartFile?):Map<String,Any>{
        return response.responseObject(usersService.uploadUserProfile(id,image!!))
    }

    // get user profile
    @Value("\${users.image}")
    var image: String? = null
    @GetMapping("/images/{fileName:.+}")
    fun getUserProfile(@PathVariable("fileName") imagesName: String, request: HttpServletRequest): ResponseEntity<*>? {
        println("getImage :$imagesName")
        return try {
            FileUpload.loadFile(imagesName, image!!, request)
        }
        catch (ex:Exception) {
            null
        }
    }

    // Apply Role to User
    @PostMapping("/apply/role/{userId}")
    fun applyRolesToUser(@PathVariable(value ="userId") userId: Long, @RequestBody rolesId: LongArray):MutableMap<String,Any>{
        return  response.responseObject(usersService.applyRolesToUser(userId,rolesId))
    }

}