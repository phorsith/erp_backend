package com.learning.spring.services.serviceImpl

import com.learning.spring.models.SalePerson
import com.learning.spring.repositories.SalePersonRepository
import com.learning.spring.services.SalePersonService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class SalePersonServiceImpl:SalePersonService {

    @Autowired
    lateinit var salePersonRepository: SalePersonRepository

    override fun findAllList(q: String?, page: Int, size: Int): Page<SalePerson>? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findById(id: Long): SalePerson? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun addNew(t: SalePerson): SalePerson? {
        return salePersonRepository.save(t)
    }

    override fun updateObj(id: Long, t: SalePerson): SalePerson? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findAll(): List<SalePerson>? {
        return salePersonRepository.findAll()
    }

}