package com.learning.spring.models


import com.learning.spring.bases.model.BaseEntity
import javax.persistence.*

@Entity
@Table(name="village")
class Village(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long ? = null,
        var village: String?="",
        @Column(name = "village_kh")
        var villageKhmer: String?="",
        var villid: String? = "",
        //@JsonIgnore
        @ManyToOne
        @JoinColumn(name="commune_id", nullable=false)
        var commune: Commune?= null

): BaseEntity()