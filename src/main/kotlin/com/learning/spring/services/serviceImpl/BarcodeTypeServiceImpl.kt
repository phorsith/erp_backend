package com.learning.spring.services.serviceImpl

import com.learning.spring.models.BarcodeType
import com.learning.spring.repositories.BarcodeTypeRepository
import com.learning.spring.services.BarcodeTypeService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class BarcodeTypeServiceImpl: BarcodeTypeService {

    @Autowired
    lateinit var barcodeTypeRepository: BarcodeTypeRepository

    override fun findAllList(q: String?, page: Int, size: Int): Page<BarcodeType>? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findById(id: Long): BarcodeType? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun addNew(t: BarcodeType): BarcodeType? {
        return barcodeTypeRepository.save(t)
    }

    override fun updateObj(id: Long, t: BarcodeType): BarcodeType? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findAll(): List<BarcodeType>? {
        return barcodeTypeRepository.findAllByStatusTrueOrderByIdDesc()
    }
}