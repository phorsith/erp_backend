package com.learning.spring.controller

import com.learning.spring.models.Barcode
import com.learning.spring.services.BarcodeService
import com.learning.spring.utils.ResponseObjectMap
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/barcode")
class BarcodeController {

    @Autowired
    lateinit var barcodeService: BarcodeService
    val response = ResponseObjectMap()

    @PostMapping()
    fun addBarcode(@RequestBody barcode: Barcode): MutableMap<String,Any>
        = response.responseObject(barcodeService.addNew(barcode))

    @GetMapping("/all")
    fun getAllBarcode(): MutableMap<String,Any> =
        response.responseObject(barcodeService.findAll())
}