package com.learning.spring.repositories

import com.learning.spring.models.ItemBranch
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor

interface ItemBranchRepository: JpaRepository<ItemBranch,Long>,JpaSpecificationExecutor<ItemBranch> {
    fun findAllByStatusTrueOrderByIdDesc(): List<ItemBranch>?
}