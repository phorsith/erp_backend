package com.learning.spring.services.serviceImpl

import com.learning.spring.models.Driver
import com.learning.spring.repositories.DriverRepository
import com.learning.spring.services.DriverService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Service
import javax.persistence.criteria.Predicate

@Service
class DriverServiceImpl: DriverService {
    @Autowired
    lateinit var driverRepository: DriverRepository

    override fun findAllList(q: String?, page: Int, size: Int): Page<Driver>? {
        return driverRepository.findAll({root, query, cb ->
            val predicates = ArrayList<Predicate>()
            if (q != null) {
                val name = cb.like(cb.upper(root.get("name")), "%${q.toUpperCase()}%")
                val email = cb.like(cb.upper(root.get("email")), "%${q.toUpperCase()}%")
                predicates.add(cb.or( name, email))
            }
            predicates.add(cb.equal(root.get<String>("status"), true))
            query.orderBy(cb.desc(root.get<String>("id")))
            cb.and(*predicates.toTypedArray())
        }, PageRequest.of(page, size))
    }

    override fun findById(id: Long): Driver? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun addNew(t: Driver): Driver? {
        return driverRepository.save(t)
    }

    override fun updateObj(id: Long, t: Driver): Driver? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findAll(): List<Driver>? {
        return driverRepository.findAllByStatusTrueOrderByIdDesc()
    }
}