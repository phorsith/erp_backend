package com.learning.spring.controller

import com.learning.spring.models.Territory
import com.learning.spring.services.TerritoryService
import com.learning.spring.utils.ResponseObjectMap
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/territory")
class TerritoryController {
    @Autowired
    lateinit var territoryService: TerritoryService

    val response = ResponseObjectMap()

    @PostMapping()
    fun addNew(@RequestBody territory: Territory): MutableMap<String,Any>
        = response.responseObject(territoryService.addNew(territory))

    @GetMapping("/all")
    fun getAllTerritory(): MutableMap<String,Any> = response.responseObject(territoryService.findAll())
}