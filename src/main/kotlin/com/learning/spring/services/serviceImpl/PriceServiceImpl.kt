package com.learning.spring.services.serviceImpl

import com.learning.spring.models.Price
import com.learning.spring.repositories.PriceRepository
import com.learning.spring.services.PriceService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class PriceServiceImpl:PriceService {
    @Autowired
    lateinit var priceRepository: PriceRepository

    override fun findAllList(q: String?, page: Int, size: Int): Page<Price>? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findById(id: Long): Price? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun addNew(t: Price): Price? {
        return priceRepository.save(t)
    }

    override fun updateObj(id: Long, t: Price): Price? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findAll(): List<Price>? {
        return priceRepository.findAllByStatusTrueOrderByIdDesc()
    }

}