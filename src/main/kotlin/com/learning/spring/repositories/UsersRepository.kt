package com.learning.spring.repositories

import com.learning.spring.models.Users
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository

@Repository
interface UsersRepository: JpaRepository<Users,Long>,JpaSpecificationExecutor<Users> {
    fun findByIdAndStatusTrue(id:Long): Users?
    fun findAllByStatusTrueOrderByIdDesc():List<Users>?
    fun findByUserNameOrEmailAndStatusTrue(username: String, email: String):Users?

}