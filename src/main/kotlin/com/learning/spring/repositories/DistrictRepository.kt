package com.learning.spring.repositories

import com.learning.spring.models.District
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository

@Repository
interface DistrictRepository: JpaRepository<District,Any>,JpaSpecificationExecutor<District> {
    // fun findByIdAndStatusTrue(id:Long): Users?
    fun findByProvinceIdAndStatusTrue(provId: Long): List<District>
    //fun
}