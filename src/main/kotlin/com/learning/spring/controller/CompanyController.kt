package com.learning.spring.controller

import com.learning.spring.models.Company
import com.learning.spring.models.Currency
import com.learning.spring.services.CompanyService
import com.learning.spring.services.CurrencyService
import com.learning.spring.utils.ResponseObjectMap
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/company")
class CompanyController {
    @Autowired
    lateinit var companyService: CompanyService
    val response = ResponseObjectMap()

    @PostMapping()
    fun addCompany(@RequestBody company: Company): MutableMap<String,Any> =
        response.responseObject(companyService.addNew(company))

    @GetMapping("/all")
    fun getAllCompany(): MutableMap<String,Any> =
        response.responseObject(companyService.findAll())

    @GetMapping("/list")
    fun findAllCompanyCriteria(@RequestParam(required = false) query: String?, size: Int, page: Int) : MutableMap<String, Any> {
        val itemPage = companyService.findAllList(query, page, size)
        val item = itemPage?.content
        val totalElements = itemPage?.totalElements
        return response.responseObject(item, totalElements!!)
    }

}