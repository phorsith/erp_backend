package com.learning.spring.repositories

import com.learning.spring.models.Price
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository

@Repository
interface PriceRepository:JpaRepository<Price,Long>,JpaSpecificationExecutor<Price> {
    fun findAllByStatusTrueOrderByIdDesc(): List<Price>?
}