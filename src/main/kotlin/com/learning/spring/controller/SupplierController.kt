package com.learning.spring.controller

import com.learning.spring.models.Supplier
import com.learning.spring.services.SupplierService
import com.learning.spring.utils.ResponseObjectMap
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/supplier")
class SupplierController {
    @Autowired
    lateinit var supplierService: SupplierService

    val response = ResponseObjectMap()

    @PostMapping()
    fun addSupplier(@RequestBody supplier: Supplier): MutableMap<String,Any> =
            response.responseObject(supplierService.addNew(supplier))

    @GetMapping("/all")
    fun getAllSupplier(): MutableMap<String,Any> =
            response.responseObject(supplierService.findAll())

}