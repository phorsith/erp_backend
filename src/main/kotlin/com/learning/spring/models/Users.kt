package com.learning.spring.models

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.learning.spring.bases.model.BaseEntity
import javax.persistence.*

@Entity
@Table( name = "users")
class Users (
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id : Long ? = 0,
        @Column( name = "first_name", length = 50)
        var firstName : String ?= "",
        @Column( name ="last_name", length = 50)
        var lastName : String ?= "",
        @Column( name ="full_name", length = 50)
        var fullName : String ?= "",
        @Column( name ="username", length = 50)
        var userName : String ?= "",
        var email : String ? = "",
        var gender : String ? = "",
        @Column( name ="phone_number", length = 50)
        var phoneNo : String? = "",
        @JsonIgnoreProperties(allowSetters = true)
        var password: String?="",
        @Column( name ="image_part")
        var imagePart: String? = null,
        var image: String?="",
        @JsonIgnore
        @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
        var userRole: MutableList<UserRole> ? = null
) : BaseEntity()

class LoginRequest(

        var userName: String?="",
        var password: String?= ""
)