package com.learning.spring.services

import com.learning.spring.models.CustomerDropdown


interface CustomerDropdownService {
    //fun getDropDownList(): CashierAssociation?
    fun getCustomerDropdownList(): CustomerDropdown?
}