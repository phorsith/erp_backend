package com.learning.spring.services.serviceImpl

import com.learning.spring.models.CustomerDropdown
import com.learning.spring.services.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CustomerDropdownServiceImpl: CustomerDropdownService{
    //@Autowired lateinit var roleService: RoleServiceImp
    @Autowired
    lateinit var genderService: GenderServiceImpl
    @Autowired
    lateinit var salutationService: SalutationServiceImpl
    @Autowired
    lateinit var customerGroupService: CustomerGroupServiceImpl
    @Autowired
    lateinit var customerTypeService: CustomerTypeServiceImpl
    @Autowired
    lateinit var territoryService: TerritoryServiceImpl
    @Autowired
    lateinit var warehouseService: DeliveryWarehouseService
    @Autowired
    lateinit var paymentTermService: PaymentTermServiceImpl
    @Autowired
    lateinit var currencyService: CurrencyServiceImpl
    @Autowired
    lateinit var priceService: PriceServiceImpl
    @Autowired
    lateinit var salePersonService: SalePersonServiceImpl

    override fun getCustomerDropdownList(): CustomerDropdown?{
        //val allThingObj = CashierAssociation()
        val customerDropdown =CustomerDropdown()
        customerDropdown.genderList = genderService.findAll()
        customerDropdown.salutationList = salutationService.findAll()
        customerDropdown.customerGroupList = customerGroupService.findAll()
        customerDropdown.customerTypeList = customerTypeService.findAll()
        customerDropdown.territoryList = territoryService.findAll()
        customerDropdown.warehouseList = warehouseService.findAll()
        customerDropdown.paymentTermList = paymentTermService.findAll()
        customerDropdown.currencyList = currencyService.findAll()
        customerDropdown.priceListList = priceService.findAll()
        customerDropdown.salePersonList = salePersonService.findAll()

        return customerDropdown
    }
}