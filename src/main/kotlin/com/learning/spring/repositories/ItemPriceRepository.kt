package com.learning.spring.repositories

import com.learning.spring.models.ItemPrice
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository

@Repository
interface ItemPriceRepository: JpaRepository<ItemPrice,Long>,JpaSpecificationExecutor<ItemPrice> {
    fun findAllByStatusTrueOrderByIdDesc(): List<ItemPrice>?
}