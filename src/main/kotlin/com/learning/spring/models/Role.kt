package com.learning.spring.models

import com.fasterxml.jackson.annotation.JsonIgnore
import com.learning.spring.bases.model.BaseEntity
import javax.persistence.*

@Entity
@Table(name="role")
class Role(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long ? = null,
        @Column(name = "role_name")
        var roleName: String?="",
        var description: String?="",
        @JsonIgnore
        @OneToMany(mappedBy = "role",cascade = [CascadeType.ALL], fetch = FetchType.LAZY)
        var userRole: MutableList<UserRole> ? = null

): BaseEntity()