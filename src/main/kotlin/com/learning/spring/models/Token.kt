package com.learning.spring.models

import com.learning.spring.bases.model.BaseEntity
import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

@Entity
class Token(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long? = 0,
        var token:String? = null,
        var expireIn: Date? = null
) : BaseEntity()