package com.learning.spring.services.serviceImpl

import com.learning.spring.models.Currency
import com.learning.spring.repositories.CurrencyRepository
import com.learning.spring.services.CurrencyService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class CurrencyServiceImpl:CurrencyService {
    @Autowired
    lateinit var currencyRepository: CurrencyRepository

    override fun findAllList(q: String?, page: Int, size: Int): Page<Currency>? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findById(id: Long): Currency? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun addNew(t: Currency): Currency? {
        return currencyRepository.save(t)
    }

    override fun updateObj(id: Long, t: Currency): Currency? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findAll(): List<Currency>? {
       return currencyRepository.findAllByStatusTrueOrderByIdDesc()
    }
}