package com.learning.spring.models

import com.learning.spring.bases.model.BaseEntity
import javax.persistence.*

@Entity
@Table(name="item_inventory")
class ItemInventory (
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long ? = null,
        @Column(name="self_in_day")
        var selfLifeInDay: Int? = null,
        @Column(name="warranty_period")
        var warrantyPeriod: Int? = null,
        @Column(name="end_of_life")
        var endOfLife: Int? = null,
        @Column(name="weight_per_unit")
        var weightPerUnit: Int? = null
): BaseEntity()