package com.learning.spring.repositories

import com.learning.spring.models.Currency
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository

@Repository
interface CurrencyRepository:JpaRepository<Currency,Long>,JpaSpecificationExecutor<Currency> {
    fun findAllByStatusTrueOrderByIdDesc(): List<Currency>?

}