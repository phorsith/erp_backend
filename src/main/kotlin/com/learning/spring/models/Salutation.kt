package com.learning.spring.models

import com.learning.spring.bases.model.BaseEntity
import javax.persistence.*

@Entity
@Table(name="salutation")
class Salutation (
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long ? = null,
        var salutation: String?=""
//        @OneToOne
//        @JoinColumn(name = "gender_id", nullable = false)
//        var gender: Gender ?= null
): BaseEntity()

//class SalutationData(
//        var salutation: Salutation? = null,
//        var genderId: Int ? = null
//)