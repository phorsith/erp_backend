package com.learning.spring.controller

import com.learning.spring.models.ItemVariantUom
import com.learning.spring.services.ItemVariantUomService
import com.learning.spring.utils.ResponseObjectMap
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("item-variant-uom")
class ItemVariantUomController {
    @Autowired
    lateinit var itemVariantUomService: ItemVariantUomService
    val response = ResponseObjectMap()
    @GetMapping("/all")
    fun getAllItemVariantUom(): MutableMap<String,Any> = response.responseObject(itemVariantUomService.findAll())

    @PostMapping()
    fun  addItemVariantUom(@RequestBody itemVariantUom: ItemVariantUom): MutableMap<String,Any> =
            response.responseObject(itemVariantUomService.addNew(itemVariantUom))
}