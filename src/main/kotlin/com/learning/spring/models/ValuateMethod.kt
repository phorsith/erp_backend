package com.learning.spring.models

import com.learning.spring.bases.model.BaseEntity
import javax.persistence.*

@Entity
@Table(name="valuate_mathod")
class ValuateMethod(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long ? = null,
        @Column(name="valuate_method")
        var valuateMethod: String? = "",
        var description: String? = ""
): BaseEntity()