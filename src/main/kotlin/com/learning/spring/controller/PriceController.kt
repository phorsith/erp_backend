package com.learning.spring.controller

import com.learning.spring.models.Price
import com.learning.spring.services.PriceService
import com.learning.spring.utils.ResponseObjectMap
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/price")
class PriceController {
    @Autowired
    lateinit var priceService: PriceService
    val response = ResponseObjectMap()

    @PostMapping()
    fun addPrice(@RequestBody price: Price): MutableMap<String,Any> =
            response.responseObject(priceService.addNew(price))

    @GetMapping("/all")
    fun getAllPrice(): MutableMap<String,Any> = response.responseObject(priceService.findAll())

}
