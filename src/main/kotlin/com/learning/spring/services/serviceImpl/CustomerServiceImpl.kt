package com.learning.spring.services.serviceImpl

import com.learning.spring.models.Customer
import com.learning.spring.repositories.CustomerRepository
import com.learning.spring.services.CustomerService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Service
import javax.persistence.criteria.Predicate

@Service
class CustomerServiceImpl: CustomerService {
    @Autowired
    lateinit var customerRepository: CustomerRepository

    override fun findAllList(q: String?, page: Int, size: Int): Page<Customer>? {
        return customerRepository.findAll({root, query, cb ->
            val predicates = ArrayList<Predicate>()
            if (q != null) {
                val name = cb.like(cb.upper(root.get("name")), "%${q.toUpperCase()}%")
                val email = cb.like(cb.upper(root.get("email")), "%${q.toUpperCase()}%")
                predicates.add(cb.or( name, email))
            }
            predicates.add(cb.equal(root.get<String>("status"), true))
            query.orderBy(cb.desc(root.get<String>("id")))
            cb.and(*predicates.toTypedArray())
        }, PageRequest.of(page, size))
    }

    override fun findById(id: Long): Customer? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun addNew(t: Customer): Customer? {
        return customerRepository.save(t)
    }

    override fun updateObj(id: Long, t: Customer): Customer? {
        val customer = customerRepository.findByIdAndStatusTrue(id)
        if (!t.status) customer!!.status = false

        customer?.name = t.name
        customer?.nameKhmer = t.nameKhmer
        customer?.phoneNumber = t.phoneNumber
        customer?.houseNumber = t.houseNumber
        customer?.street = t.street
        customer?.address = t.address
        customer?.villingAddress = t.villingAddress
        customer?.shippingAddress = t.shippingAddress
        customer?.disable = t.disable
        customer?.internalCustomer = t.internalCustomer
        customer?.creditLimitCheck = t.creditLimitCheck
        customer?.customerGroup = t.customerGroup
        customer?.gender = t.gender
        customer?.salutation = t.salutation
        customer?.customerType = t.customerType
        customer?.currency = t.currency
        customer?.paymentTerm = t.paymentTerm
        customer?.price = t.price
        customer?.deliveryWarehouse = t.deliveryWarehouse
        customer?.territory = t.territory
        customer?.salePerson = t.salePerson
        customer?.province = t.province
        customer?.district = t.district
        customer?.commune = t.commune
        customer?.village = t.village

        return customerRepository.save(customer!!)
    }

    override fun findAll(): List<Customer>? {
        return customerRepository.findAllByStatusTrueOrderByIdDesc()
    }

}