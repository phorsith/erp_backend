package com.learning.spring.services.serviceImpl

import com.learning.spring.models.Commune
import com.learning.spring.models.Village
import com.learning.spring.repositories.VillageRepository
import com.learning.spring.services.VillageService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class VillageServiceImpl: VillageService {
    @Autowired
    lateinit var villageRepository: VillageRepository

    override fun getVillageByCommuneId(commId: Long): List<Village> {
        return villageRepository.findByCommuneIdAndStatusTrue(commId)
    }

    override fun findAllList(q: String?, page: Int, size: Int): Page<Village>? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findById(id: Long): Village? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun addNew(t: Village): Village? {
       return villageRepository.save(t)
    }

    override fun updateObj(id: Long, t: Village): Village? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findAll(): List<Village>? {
        return villageRepository.findAll()
    }

}