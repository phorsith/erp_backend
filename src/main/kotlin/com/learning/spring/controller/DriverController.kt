package com.learning.spring.controller

import com.learning.spring.models.Driver
import com.learning.spring.services.DriverService
import com.learning.spring.utils.ResponseObjectMap
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/driver")
class DriverController {
    @Autowired
    lateinit var driverService: DriverService
    val response = ResponseObjectMap()

    @PostMapping()
    fun addDriver(@RequestBody driver: Driver): MutableMap<String,Any> =
            response.responseObject(driverService.addNew(driver))

    @GetMapping("/all")
    fun getAllDriver(): MutableMap<String,Any> =
            response.responseObject(driverService.findAll())

    @GetMapping("/list")
    fun findAllDriverCriteria(@RequestParam(required = false) query: String?, size: Int, page: Int) : MutableMap<String, Any> {
        val itemPage = driverService.findAllList(query, page, size)
        val item = itemPage?.content
        val totalElements = itemPage?.totalElements
        return response.responseObject(item, totalElements!!)
    }

}