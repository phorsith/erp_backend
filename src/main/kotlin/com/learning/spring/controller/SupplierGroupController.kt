package com.learning.spring.controller

import com.learning.spring.models.SupplierGroup
import com.learning.spring.services.SupplierGroupService
import com.learning.spring.utils.ResponseObjectMap
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/supplier-group")
class SupplierGroupController {
    @Autowired
    lateinit var supplierGroupService: SupplierGroupService

    val response = ResponseObjectMap()

    @PostMapping()
    fun addSupplierGroupService(@RequestBody supplierGroup: SupplierGroup): MutableMap<String,Any> =
            response.responseObject(supplierGroupService.addNew(supplierGroup))

    @GetMapping("/all")
    fun getAllSupplierGroupService(): MutableMap<String,Any> =
            response.responseObject(supplierGroupService.findAll())
}

