package com.learning.spring.services.serviceImpl

import com.learning.spring.models.Account
import com.learning.spring.repositories.AccountRepository
import com.learning.spring.services.AccountService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.persistence.criteria.Predicate
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest

@Service
class AccountServiceImpl: AccountService {
    @Autowired
    lateinit var accountRepository: AccountRepository

    override fun findAllList(q: String?, page: Int, size: Int): Page<Account>? {
        return accountRepository.findAll({root, query, cb ->
                    val predicates = ArrayList<Predicate>()
                    if (q != null) {
                        val name = cb.like(cb.upper(root.get("name")), "%${q.toUpperCase()}%")
                        val email = cb.like(cb.upper(root.get("email")), "%${q.toUpperCase()}%")
                        predicates.add(cb.or( name, email))
                    }
                    predicates.add(cb.equal(root.get<String>("status"), true))
                    query.orderBy(cb.desc(root.get<String>("id")))
                    cb.and(*predicates.toTypedArray())
                }, PageRequest.of(page, size))
    }

    override fun findById(id: Long): Account? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun addNew(t: Account): Account? {
        return accountRepository.save(t)
    }

    override fun updateObj(id: Long, t: Account): Account? {
        val account = accountRepository.findByIdAndStatusTrue(id)
                if (!t.status) account!!.status = false
            account?.accountName = t.accountName
            account?.description = t.description
            account?.accountType = t.accountType
        return accountRepository.save(account!!)

    }

    override fun findAll(): List<Account>? {
        return accountRepository.findAllByStatusTrueOrderByIdDesc()
    }
}