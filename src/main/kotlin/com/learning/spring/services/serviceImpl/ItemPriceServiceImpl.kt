package com.learning.spring.services.serviceImpl

import com.learning.spring.models.ItemPrice
import com.learning.spring.repositories.ItemPriceRepository
import com.learning.spring.services.ItemPriceService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class ItemPriceServiceImpl: ItemPriceService {

    @Autowired
    lateinit var itemPriceRepository: ItemPriceRepository

    override fun findAllList(q: String?, page: Int, size: Int): Page<ItemPrice>? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findById(id: Long): ItemPrice? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun addNew(t: ItemPrice): ItemPrice? {
        return itemPriceRepository.save(t)
    }

    override fun updateObj(id: Long, t: ItemPrice): ItemPrice? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findAll(): List<ItemPrice>? {
       return itemPriceRepository.findAllByStatusTrueOrderByIdDesc()
    }

}