package com.learning.spring.models

class ItemDropdown(
        //var itemPriceList: List<ItemPrice>? = null,
        var uomList: List<Uom>? = null,
        var itemGroupList: List<ItemGroup>? = null,
        var itemBranchList: List<ItemBranch>? = null,
        var supplierList: List<Supplier>? = null,
        var valuateMethodList: List<ValuateMethod>? = null
)