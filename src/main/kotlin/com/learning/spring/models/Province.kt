package com.learning.spring.models

import com.fasterxml.jackson.annotation.JsonIgnore
import com.learning.spring.bases.model.BaseEntity
import javax.persistence.*

@Entity
@Table(name="province")
class Province (
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long ? = null,
        var province: String?="",
        @Column(name = "province_kh")
        var provinceKhmer: String?="",
        var provid: String? = "",
        @JsonIgnore
        @OneToMany(mappedBy = "province", cascade = [CascadeType.ALL])
        var district: MutableList<District> ?= null

):BaseEntity()