package com.learning.spring.models

import com.fasterxml.jackson.annotation.JsonIgnore
import com.learning.spring.bases.model.BaseEntity
import org.mapstruct.Mapping
import javax.persistence.*

@Entity
@Table(name="item")
class Item(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long ? = null,
        @Column(name="item_name")
        var itemName: String? = "",
        @Column(name="item_code")
        var itemCode:String? = "",
        @Column(name="open_stock")
        var openStock: String? = "",
        @Column(name="standard_buying_price")
        var standardBuyingPrice: Int? = null,
        @Column(name="standard_selling_price")
        var standardSellingPrice: Int? = null,
        var description: String? = "",
        @Column(name="image_part")
        var imagePart: String? = "",
        var image: String? = "",
        @Column(name="has_batch_no")
        var hasBatchNo: Boolean? = true,
        @Column(name="has_serial_no")
        var hasSerialNo: Boolean? = true,
        @Column(name="serial_no")
        var serialNo: String? = "",
        @Column(name="max_discount")
        var maxDiscount: Int? = null,
        var cost: Int? = null,
        @Column(name="sale_unit_of_measure")
        var saleUnitOfMeasure: Int? = null,
        @Column(name="manufacture_part_number")
        var manufacturePartNumber: Int? = null,
        var disable: Boolean? = true,
        @Column(name="absolute_value")
        var absoluteValue: Boolean? = true,
        @Column(name="fixed_asset")
        var fixedAsset: Boolean? = true,
        @Column(name="maintain_stock")
        var maintainStock: Boolean? = true,
        @Column(name="is_sale_item")
        var isSaleItem: Boolean? = true,
        var barcode: String?= "",
        @OneToOne
        @JoinColumn(name="itemgroup_id", nullable = false)
        var itemGroup: ItemGroup? = null,
        @OneToOne
        @JoinColumn(name="valuate_id", nullable = false)
        var valuateMethod: ValuateMethod? = null,
        @OneToOne
        @JoinColumn(name="uom_id", nullable = false)
        var uom: Uom? = null,
        @OneToOne
        @JoinColumn(name="branch_id", nullable = false)
        var itemBranch: ItemBranch? = null,
        @OneToOne
        @JoinColumn(name="supplier_id", nullable = false)
        var supplier: Supplier? = null,
        @OneToMany(mappedBy = "item", fetch = FetchType.LAZY)
        var itemVariantUom: MutableList<ItemVariantUom>? = null


): BaseEntity()