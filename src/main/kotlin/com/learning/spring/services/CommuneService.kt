package com.learning.spring.services

import com.learning.spring.bases.service.BaseService
import com.learning.spring.models.Commune

interface CommuneService:BaseService<Commune> {
    fun getCommuneByDistrictId(distId: Long): List<Commune>
}