package com.learning.spring.models

class CustomerDropdown(
    var genderList: List<Gender>? = null,
    var salutationList: List<Salutation>? = null,
    var customerGroupList: List<CustomerGroup>? = null,
    var customerTypeList: List<CustomerType>? = null,
    var territoryList: List<Territory>?= null,
    var warehouseList: List<DeliveryWarehouse>? = null,
    var paymentTermList: List<PaymentTerm>? = null,
    var currencyList: List<Currency>? = null,
    var priceListList: List<Price>? = null,
    var salePersonList: List<SalePerson>? = null
)