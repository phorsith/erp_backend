package com.learning.spring.controller

import com.learning.spring.models.Domain
import com.learning.spring.services.DomainService
import com.learning.spring.utils.ResponseObjectMap
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/domain")
class DomainController {
    @Autowired
    lateinit var domainService: DomainService
    val response = ResponseObjectMap()

    @PostMapping()
    fun addDomain(@RequestBody domain: Domain): MutableMap<String,Any> =
            response.responseObject(domainService.addNew(domain))

    @GetMapping("/all")
    fun getAllDomain(): MutableMap<String,Any> =
            response.responseObject(domainService.findAll())

    @GetMapping("/list")
    fun findAllDomainCriteria(@RequestParam(required = false) query: String?, size: Int, page: Int) : MutableMap<String, Any> {
        val itemPage = domainService.findAllList(query, page, size)
        val item = itemPage?.content
        val totalElements = itemPage?.totalElements
        return response.responseObject(item, totalElements!!)
    }

}