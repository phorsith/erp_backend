package com.learning.spring.configurations

import com.learning.spring.utils.SwaggerConstant
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.service.ApiInfo
import springfox.documentation.service.Contact
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2

/***
 * @author PHOR Sith
 * Date: 2020/07/10
 * @Back End Developer
 */

@Configuration
@EnableSwagger2
class SpringFoxConfiguration {
    @Bean
    fun api(): Docket {
        return Docket(DocumentationType.SWAGGER_2)
            .apiInfo(apiInfo())
            .select()
            .apis(RequestHandlerSelectors.basePackage("com.learning.spring"))
            .paths(PathSelectors.any())
            .build()
            .apiInfo(apiInfo())
    }



    // Describe your apis
    private fun apiInfo(): ApiInfo {
        return ApiInfo(
            SwaggerConstant.TITLE,
            SwaggerConstant.DESCRIPTION,
            SwaggerConstant.VERSION,
            SwaggerConstant.TERM_OF_SERVICE_URL,
            Contact(SwaggerConstant.CONTACT_NAME, SwaggerConstant.CONTACT_URL, SwaggerConstant.CONTACT_EMAIL),
            SwaggerConstant.CONTACT_VERSION,
            SwaggerConstant.CONTACT_URL_VERSION)

    }
}