package com.learning.spring.repositories

import com.learning.spring.models.Item
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository

@Repository
interface ItemRepository: JpaRepository<Item,Long>,JpaSpecificationExecutor<Item> {
    fun findByIdAndStatusTrue(id:Long): Item?
    fun findAllByStatusTrueOrderByIdDesc(): List<Item>?
}