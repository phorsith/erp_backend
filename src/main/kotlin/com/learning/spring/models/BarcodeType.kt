package com.learning.spring.models

import com.learning.spring.bases.model.BaseEntity
import javax.persistence.*

@Entity
@Table(name="barcode_type")
class BarcodeType(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long ? = null,
        @Column(name="barcode_type")
        var barcodeTypeName: String ? = ""
):BaseEntity()