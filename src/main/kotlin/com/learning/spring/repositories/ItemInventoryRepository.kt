package com.learning.spring.repositories

import com.learning.spring.models.ItemInventory
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository

@Repository
interface ItemInventoryRepository: JpaRepository<ItemInventory, Long>,JpaSpecificationExecutor<ItemInventory> {
    fun findAllByStatusTrueOrderByIdDesc(): List<ItemInventory>?
}