package com.learning.spring.controller

import com.learning.spring.models.TermCondition
import com.learning.spring.services.TermConditionService
import com.learning.spring.utils.ResponseObjectMap
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/term-condition")
class TermConditionController {
    @Autowired
    lateinit var termConditionService: TermConditionService
    val response = ResponseObjectMap()

    @PostMapping()
    fun addTermCondition(@RequestBody termCondition: TermCondition): MutableMap<String,Any> =
        response.responseObject(termConditionService.addNew(termCondition))

    @GetMapping("/all")
    fun getAllTermCondition(): MutableMap<String,Any> =
        response.responseObject(termConditionService.findAll())

}