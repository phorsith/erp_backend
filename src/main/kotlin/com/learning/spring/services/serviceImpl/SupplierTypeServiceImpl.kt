package com.learning.spring.services.serviceImpl

import com.learning.spring.models.SupplierType
import com.learning.spring.repositories.BarcodeTypeRepository
import com.learning.spring.repositories.SupplierTypeRepository
import com.learning.spring.services.SupplierTypeService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class SupplierTypeServiceImpl: SupplierTypeService{
    @Autowired
    lateinit var supplierTypeRepository: SupplierTypeRepository

    override fun findAllList(q: String?, page: Int, size: Int): Page<SupplierType>? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findById(id: Long): SupplierType? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun addNew(t: SupplierType): SupplierType? {
        return supplierTypeRepository.save(t)
    }

    override fun updateObj(id: Long, t: SupplierType): SupplierType? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findAll(): List<SupplierType>? {
       return supplierTypeRepository.findAllByStatusTrueOrderByIdDesc()
    }

}