package com.learning.spring.services

import com.learning.spring.bases.service.BaseService
import com.learning.spring.models.Token

interface TokenService: BaseService<Token> {
    fun findByToken(accessToken: Token) : Token?
    fun deleteByToken(token: String) : Int
}