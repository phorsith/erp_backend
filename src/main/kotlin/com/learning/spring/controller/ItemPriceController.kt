package com.learning.spring.controller

import com.learning.spring.models.ItemPrice
import com.learning.spring.services.ItemPriceService
import com.learning.spring.utils.ResponseObjectMap
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/item-price")
class ItemPriceController {
    @Autowired
    lateinit var itemPriceService: ItemPriceService
    val response = ResponseObjectMap()

    @PostMapping()
    fun addItemPrice(@RequestBody itemPrice: ItemPrice): MutableMap<String,Any>
        = response.responseObject(itemPriceService.addNew(itemPrice))

    @GetMapping("/all")
    fun getAllItemPrice(): MutableMap<String,Any> =
        response.responseObject(itemPriceService.findAll())

}