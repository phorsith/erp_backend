package com.learning.spring.controller

import com.learning.spring.models.SupplierType
import com.learning.spring.services.SupplierTypeService
import com.learning.spring.utils.ResponseObjectMap
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/supplier-type")
class SupplierTypeController {
    @Autowired
    lateinit var supplierTypeService: SupplierTypeService

    val response = ResponseObjectMap()

    @PostMapping()
    fun addSupplierType(@RequestBody supplierType: SupplierType): MutableMap<String,Any> =
        response.responseObject(supplierTypeService.addNew(supplierType))

    @GetMapping("/all")
    fun getAllSupplierType(): MutableMap<String,Any> =
        response.responseObject(supplierTypeService.findAll())

}