package com.learning.spring.services

import com.learning.spring.bases.service.BaseService
import com.learning.spring.models.District

interface DistrictService: BaseService<District> {
    //fun applyRolesToUser(userId: Long, rolesList: LongArray): List<UserRole>
    fun getDistrictByProvinceId(provId: Long):List<District>
}