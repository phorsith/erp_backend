package com.learning.spring.services.serviceImpl

import com.learning.spring.models.ItemVariantUom
import com.learning.spring.repositories.ItemVariantUomRepository
import com.learning.spring.services.ItemVariantUomService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class ItemVariantUomServiceImpl: ItemVariantUomService {

    @Autowired
    lateinit var itemVariantUomRepository: ItemVariantUomRepository

    override fun findAllList(q: String?, page: Int, size: Int): Page<ItemVariantUom>? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findById(id: Long): ItemVariantUom? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun addNew(t: ItemVariantUom): ItemVariantUom? {
        return itemVariantUomRepository.save(t)
    }

    override fun updateObj(id: Long, t: ItemVariantUom): ItemVariantUom? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findAll(): List<ItemVariantUom>? {
        return itemVariantUomRepository.findByStatusTrueOrderByIdDesc()
    }
}