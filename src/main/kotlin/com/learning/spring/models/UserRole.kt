package com.learning.spring.models

import com.learning.spring.bases.model.BaseEntity
import javax.persistence.*

@Entity
@Table(name="user_role")
class UserRole (
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long? = 0,
        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "user_id", nullable = false)
        var user: Users? = null,
        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "role_id",nullable = false)
        var role: Role? = null
) : BaseEntity()