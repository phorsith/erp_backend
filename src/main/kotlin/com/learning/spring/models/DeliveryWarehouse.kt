package com.learning.spring.models

import com.learning.spring.bases.model.BaseEntity
import javax.persistence.*

@Entity
@Table(name="delivery_warehouse")
class DeliveryWarehouse (
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long ? = null,
        @Column(name="warehouse_name")
        var warehouseName: String? = "",
        var phone: Int? = null,
        @Column(name="address_line1")
        var addressLine1 : String? = "",
        @Column(name="address_line2")
        var addressLine2 : String? = ""
): BaseEntity()