package com.learning.spring.security

import com.learning.spring.exceptions.ResourceNotAcceptableException
import com.learning.spring.repositories.RoleRepository
import com.learning.spring.repositories.UsersRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Service
import java.util.stream.Collectors.*
import javax.transaction.Transactional

@Service
class CusUserDetailsService : UserDetailsService {
    @Autowired
    private lateinit var userRepository : UsersRepository
    @Autowired
    private lateinit var roleRepository: RoleRepository

    @Transactional
    override fun loadUserByUsername(userNameOrEmail: String): UserDetails {
        val user = userRepository.findByUserNameOrEmailAndStatusTrue(userNameOrEmail,userNameOrEmail) ?:
        throw ResourceNotAcceptableException("UserName or Email email $userNameOrEmail not exist.")
        return UserPrincipal.create(user.id!!,user.userName,user.email,user.password!!,getAuthority(user.id!!))
    }

    @Transactional
    fun loadUserById(id: Long): UserDetails {
        println("user , $id")
        val user = userRepository.findByIdAndStatusTrue(id) ?: throw ResourceNotAcceptableException("id $id not exist")
        return UserPrincipal.create(user.id!!,user.userName,user.email,user.password!!,getAuthority(id))
    }

    private fun getAuthority(userId: Long): MutableList<SimpleGrantedAuthority>? {
        return roleRepository.findByUserId(userId)?.stream()?.map { role -> SimpleGrantedAuthority(role.roleName) }?.collect(toList())
    }
}
