package com.learning.spring.services.serviceImpl

import com.learning.spring.models.Territory
import com.learning.spring.repositories.TerritoryRepository
import com.learning.spring.services.TerritoryService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class TerritoryServiceImpl: TerritoryService {

    @Autowired
    lateinit var territoryRepository: TerritoryRepository

    override fun findAllList(q: String?, page: Int, size: Int): Page<Territory>? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findById(id: Long): Territory? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun addNew(t: Territory): Territory? {
        return territoryRepository.save(t)
    }

    override fun updateObj(id: Long, t: Territory): Territory? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findAll(): List<Territory>? {
        return territoryRepository.findAll()
    }
}