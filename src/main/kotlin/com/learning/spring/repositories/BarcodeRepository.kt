package com.learning.spring.repositories

import com.learning.spring.models.Barcode
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository

@Repository
interface BarcodeRepository: JpaRepository<Barcode, Long>,JpaSpecificationExecutor<Barcode> {
    fun findAllByStatusTrueOrderByIdDesc(): List<Barcode>?
}