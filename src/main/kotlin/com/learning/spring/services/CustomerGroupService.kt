package com.learning.spring.services

import com.learning.spring.bases.service.BaseService
import com.learning.spring.models.CustomerGroup

interface CustomerGroupService:BaseService<CustomerGroup> {
}