package com.learning.spring.controller

import com.learning.spring.models.District
import com.learning.spring.services.DistrictService
import com.learning.spring.utils.ResponseObjectMap
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/district")
class DistrictController {
    @Autowired
    lateinit var districtService: DistrictService
    val response = ResponseObjectMap()

    @GetMapping("/all")
    fun getAllDistrict(): MutableMap<String,Any> = response.responseObject(districtService.findAll())

    @PostMapping()
    fun addDistrict(@RequestBody district: District): MutableMap<String,Any> =
            response.responseObject(districtService.addNew(district))
    @GetMapping("/province/{provId}")
    fun getDistrictByProvinceId(@PathVariable(value ="provId") provId: Long):MutableMap<String,Any> =
            response.responseObject(districtService.getDistrictByProvinceId(provId))


}