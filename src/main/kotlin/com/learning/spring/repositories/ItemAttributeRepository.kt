package com.learning.spring.repositories

import com.learning.spring.models.ItemAttribute
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository

@Repository
interface ItemAttributeRepository: JpaRepository<ItemAttribute,Long>,JpaSpecificationExecutor<ItemAttribute> {
    fun findAllByStatusTrueOrderByIdDesc(): List<ItemAttribute>?
}