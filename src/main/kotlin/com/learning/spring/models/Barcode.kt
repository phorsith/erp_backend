package com.learning.spring.models

import com.learning.spring.bases.model.BaseEntity
import javax.persistence.*

@Entity
@Table(name="barcode")
class Barcode(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long ? = null,
        var barcode: String ? = "",
        @OneToOne
        @JoinColumn(name = "barcodetype_id", nullable = false)
        var barcodeType: BarcodeType? = null
):BaseEntity()