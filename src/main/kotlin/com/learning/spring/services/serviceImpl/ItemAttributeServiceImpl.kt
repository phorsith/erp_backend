package com.learning.spring.services.serviceImpl

import com.learning.spring.models.ItemAttribute
import com.learning.spring.repositories.ItemAttributeRepository
import com.learning.spring.services.ItemAttributeService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class ItemAttributeServiceImpl: ItemAttributeService {
    @Autowired
    lateinit var itemAttributeRepository: ItemAttributeRepository

    override fun findAllList(q: String?, page: Int, size: Int): Page<ItemAttribute>? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findById(id: Long): ItemAttribute? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun addNew(t: ItemAttribute): ItemAttribute? {
        return itemAttributeRepository.save(t)
    }

    override fun updateObj(id: Long, t: ItemAttribute): ItemAttribute? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findAll(): List<ItemAttribute>? {
        return itemAttributeRepository.findAllByStatusTrueOrderByIdDesc()
    }
}