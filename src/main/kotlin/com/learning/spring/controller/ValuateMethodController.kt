package com.learning.spring.controller

import com.learning.spring.models.ValuateMethod
import com.learning.spring.services.ValuateMethodService
import com.learning.spring.utils.ResponseObjectMap
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/valuate_method")
class ValuateMethodController {
    @Autowired
    lateinit var  valuateMethodService: ValuateMethodService
    val response = ResponseObjectMap()

    @PostMapping()
    fun addItem(@RequestBody valuateMethod: ValuateMethod): MutableMap<String,Any>
            = response.responseObject(valuateMethodService.addNew(valuateMethod))

    @GetMapping("/all")
    fun getAllItem(): MutableMap<String,Any> = response.responseObject(valuateMethodService.findAll())


}