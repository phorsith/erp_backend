package com.learning.spring.controller

import com.learning.spring.models.BarcodeType
import com.learning.spring.services.BarcodeTypeService
import com.learning.spring.utils.ResponseObjectMap
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/barcode-type")
class BarcodeTypeController {

    @Autowired
    lateinit var barcodeTypeService: BarcodeTypeService
    val response = ResponseObjectMap()

    @PostMapping()
    fun addBarcodeType(@RequestBody barcodeType: BarcodeType): MutableMap<String,Any>
        = response.responseObject(barcodeTypeService.addNew(barcodeType))

    @GetMapping("/all")
    fun getAllBarcodeType(): MutableMap<String,Any> =
        response.responseObject(barcodeTypeService.findAll())
}