package com.learning.spring.repositories

import com.learning.spring.models.TermCondition
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository

@Repository
interface TermConditionRepository: JpaRepository<TermCondition, Long>,JpaSpecificationExecutor<TermCondition> {
    fun findAllByStatusTrueOrderByIdDesc(): List<TermCondition>?
}