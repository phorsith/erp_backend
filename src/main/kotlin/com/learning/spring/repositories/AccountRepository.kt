package com.learning.spring.repositories

import com.learning.spring.models.Account
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository

@Repository
interface AccountRepository:JpaRepository<Account, Long>,JpaSpecificationExecutor<Account> {
    fun findByIdAndStatusTrue(id: Long): Account?
    fun findAllByStatusTrueOrderByIdDesc():List<Account>?
}