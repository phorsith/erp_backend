package com.learning.spring.repositories

import com.learning.spring.models.Company
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository

@Repository
interface CompanyRepository: JpaRepository<Company, Long>,JpaSpecificationExecutor<Company> {
    fun findAllByStatusTrueOrderByIdDesc(): List<Company>?
}