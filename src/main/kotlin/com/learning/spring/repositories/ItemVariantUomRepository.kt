package com.learning.spring.repositories

import com.learning.spring.models.ItemVariantUom
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository

@Repository
interface ItemVariantUomRepository: JpaRepository<ItemVariantUom, Long>, JpaSpecificationExecutor<ItemVariantUom> {
    fun findByStatusTrueOrderByIdDesc(): List<ItemVariantUom>?
}