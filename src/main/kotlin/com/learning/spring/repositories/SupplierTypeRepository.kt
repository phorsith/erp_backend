package com.learning.spring.repositories

import com.learning.spring.models.SupplierType
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository

@Repository
interface SupplierTypeRepository: JpaRepository<SupplierType,Long> ,JpaSpecificationExecutor<SupplierType>{
    fun findAllByStatusTrueOrderByIdDesc(): List<SupplierType>?
}