package com.learning.spring.services.serviceImpl

import com.learning.spring.models.ItemBranch
import com.learning.spring.repositories.ItemBranchRepository
import com.learning.spring.services.ItemBranchService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class ItemBranchServiceImpl:ItemBranchService {
    @Autowired
    lateinit var itemBranchRepository: ItemBranchRepository

    override fun findAllList(q: String?, page: Int, size: Int): Page<ItemBranch>? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findById(id: Long): ItemBranch? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun addNew(t: ItemBranch): ItemBranch? {
        return itemBranchRepository.save(t)
    }

    override fun updateObj(id: Long, t: ItemBranch): ItemBranch? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findAll(): List<ItemBranch>? {
        return itemBranchRepository.findAllByStatusTrueOrderByIdDesc()
    }
}