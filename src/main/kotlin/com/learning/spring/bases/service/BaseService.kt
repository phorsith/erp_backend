package com.learning.spring.bases.service

import org.springframework.data.domain.Page

/***
 * @author PHOR Sith
 * Date: 2020/07/10
 * @Back End Developer
 */

interface BaseService<T> {
    fun findAllList(q: String?, page: Int, size: Int): Page<T>?
    fun findById(id: Long): T?
    fun addNew(t: T): T?
    fun updateObj(id: Long, t: T): T?
    fun findAll(): List<T>?

}