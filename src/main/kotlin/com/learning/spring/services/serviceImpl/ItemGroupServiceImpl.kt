package com.learning.spring.services.serviceImpl

import com.learning.spring.models.ItemGroup
import com.learning.spring.repositories.ItemGroupRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service
import com.learning.spring.services.ItemGroupService as ItemGroupService1

@Service
class ItemGroupServiceImpl: ItemGroupService1 {
    @Autowired
    lateinit var itemGroupRepository: ItemGroupRepository
    override fun findAllList(q: String?, page: Int, size: Int): Page<ItemGroup>? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findById(id: Long): ItemGroup? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun addNew(t: ItemGroup): ItemGroup? {
        return itemGroupRepository.save(t)
    }

    override fun updateObj(id: Long, t: ItemGroup): ItemGroup? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findAll(): List<ItemGroup>? {
        return itemGroupRepository.findAllByStatusTrueOrderByIdDesc()
    }

}