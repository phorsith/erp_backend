package com.learning.spring.services

import com.learning.spring.bases.service.BaseService
import com.learning.spring.models.BarcodeType

interface BarcodeTypeService: BaseService<BarcodeType> {
}