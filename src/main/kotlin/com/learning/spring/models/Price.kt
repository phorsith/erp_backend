package com.learning.spring.models

import com.learning.spring.bases.model.BaseEntity
import javax.persistence.*

@Entity
@Table(name="price")
class Price(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long ? = null,
    @Column(name="price_name")
    var priceName: String ? = ""
): BaseEntity()