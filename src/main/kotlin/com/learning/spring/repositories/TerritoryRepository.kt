package com.learning.spring.repositories

import com.learning.spring.models.Territory
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository

@Repository
interface TerritoryRepository: JpaRepository<Territory,Long>,JpaSpecificationExecutor<Territory> {
}