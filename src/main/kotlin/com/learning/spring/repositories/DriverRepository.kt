package com.learning.spring.repositories

import com.learning.spring.models.Driver
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository

@Repository
interface DriverRepository: JpaRepository<Driver, Long>,JpaSpecificationExecutor<Driver> {
    fun findAllByStatusTrueOrderByIdDesc(): List<Driver>?
}