package com.learning.spring.repositories

import com.learning.spring.models.Gender
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository

@Repository
interface GenderRepository:JpaRepository<Gender,Long>,JpaSpecificationExecutor<Gender> {
}