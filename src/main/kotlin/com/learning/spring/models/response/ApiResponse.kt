package com.learning.spring.models.response

import com.learning.spring.exceptions.ResourceNotFoundException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity

class ApiResponse {
    fun response(status:Int?): ResponseEntity<Any>{
        val map = HashMap<String,Any>()
        var message = "OK"
        var success = true
        var httpStatus = HttpStatus.OK
        when(status){
            401 -> {message = "Unauthorized.";success = false;httpStatus = HttpStatus.UNAUTHORIZED}
            404 -> {message = "Not found."; success= false;httpStatus = HttpStatus.NOT_FOUND}
        }
        map["success"]=success
        map["message"]=message
        return ResponseEntity(map,httpStatus)
    }
    fun response(key:String,vararg value:Any) : ResponseEntity<Any>{
        val data = key.split(",".toRegex())
        val map = HashMap<String,Any>()
        if(!(data.isNotEmpty() && value.isNotEmpty() && data.size == value.size)){
            throw ResourceNotFoundException("---<ApiResponse>---(response)---null")
        }else{
            val m = HashMap<String,Any>()
            data.forEachIndexed { index, s ->
                m[s] = value[index]
            }
            map["data"]=m
            map["success"]=true
            map["message"]="ok"
            return ResponseEntity(map,HttpStatus.OK)
        }
    }
}
