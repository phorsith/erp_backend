package com.learning.spring.repositories

import com.learning.spring.models.CustomerType
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository

@Repository
interface CustomerTypeRepository: JpaRepository<CustomerType,Long>,JpaSpecificationExecutor<CustomerType> {
}