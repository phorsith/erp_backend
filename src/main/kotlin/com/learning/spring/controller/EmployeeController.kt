package com.learning.spring.controller

import com.learning.spring.models.Employee
import com.learning.spring.services.EmployeeService
import com.learning.spring.utils.ResponseObjectMap
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/employee")
class EmployeeController {
    @Autowired
    lateinit var employeeService: EmployeeService
    val response = ResponseObjectMap()

    @PostMapping()
    fun addEmployee(@RequestBody employee: Employee): MutableMap<String,Any> =
        response.responseObject(employeeService.addNew(employee))

    @GetMapping("/all")
    fun getAllEmployee(): MutableMap<String,Any> =
        response.responseObject(employeeService.findAll())

}