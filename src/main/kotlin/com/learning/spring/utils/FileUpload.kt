package com.learning.spring.utils

import com.learning.spring.exceptions.CustomException
import org.springframework.core.io.Resource
import org.springframework.core.io.UrlResource
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.multipart.MultipartFile
import java.io.File
import java.io.IOException
import java.net.MalformedURLException
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.StandardCopyOption
import java.util.*
import javax.servlet.http.HttpServletRequest

class FileUpload {
    companion object{    ///singleton
        // Can  call class to using other where like singleton
        fun storeImage(file: MultipartFile?, filePath:String?):String{
            val path = Paths.get(filePath).toAbsolutePath().normalize()
            val directory = File(path.toString())
            if (!directory.exists()) {
                directory.mkdirs()
            }

            val extension = file?.originalFilename.toString()
            val sub = extension.substring(extension.lastIndexOf("."))

            val fileName = UUID.randomUUID().toString().plus(sub)

            try {
                if (file != null) {
                    Files.copy(file.inputStream, path.resolve(fileName), StandardCopyOption.REPLACE_EXISTING)
                }
            } catch (e: IOException) {
                return null.toString()
            }
            return fileName
        }

        fun removeImage(fileName : String,fileProperty:String?): Boolean {
            val path = Paths.get(fileProperty).toAbsolutePath().normalize()
            val filePath = path.resolve(fileName).normalize()
            val file = File(filePath.toString())

            return if(file.exists()) file.delete()
            else false
        }

        fun loadFile(fileName:String, fileProperty:String, request: HttpServletRequest) : ResponseEntity<Resource> {
            try {
                val path = Paths.get(fileProperty).toAbsolutePath().normalize()
                val filePath = path.resolve(fileName).normalize()
                val resource = UrlResource(filePath.toUri())

                resource.contentLength()

                val contentType = request.servletContext.getMimeType(resource.file.absolutePath)
                        ?: throw CustomException(500, "Invalid file type.")
                return ResponseEntity.ok()

                        .contentType(MediaType.parseMediaType(contentType))
                        .body<Resource>(resource)

            } catch (e: MalformedURLException) {
                throw CustomException(500, e.message)
            } catch (e: IOException) {
                throw CustomException(404, "File not found.")
            }
        }
    }
}