package com.learning.spring.security

import com.learning.spring.models.Token
import com.learning.spring.services.TokenService
import io.jsonwebtoken.*
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Component
import java.util.*

@Component
class JwtTokenProvider {
    @Value("\${app.jwtSecret}")
    private lateinit var jwtSecret: String

    @Autowired
    private lateinit var tokenService: TokenService

    private val logger = LoggerFactory.getLogger(JwtTokenProvider::class.java)

    fun generateToken(authentication: Authentication): Token {

        val userPrincipal = authentication.principal as UserPrincipal
        println("print: ${userPrincipal.id}")

        val calender = Calendar.getInstance()
        calender.add(Calendar.DAY_OF_MONTH,20)
        val expiredDate = calender.time

        val token =  Jwts.builder()
                .setSubject(userPrincipal.id.toString())
                .setIssuedAt(Date())
                .setExpiration(expiredDate)
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact()

        //save token

        tokenService.addNew(Token(null,token, expiredDate))

        return Token(null,token,expiredDate)
    }

    fun refreshToken(token: String) : Token{

        val userId = getUserIdFromJWT(token)


        val calender = Calendar.getInstance()
        calender.add(Calendar.MONTH,1)
        val expiredDate = calender.time

        val token = Jwts.builder()
                .setSubject(userId.toString())
                .setIssuedAt(Date())
                .setExpiration(expiredDate)
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact()
        return Token(null,token,expiredDate)
    }

    fun getUserIdFromJWT(token: String?): Long {

        val claims = Jwts.parser()
                .setSigningKey(jwtSecret)
                .parseClaimsJws(token)
                .body

        return java.lang.Long.parseLong(claims.subject)
    }

    fun validateToken(authToken: String?): Boolean {
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken)
            return true
        } catch (ex: SignatureException) {
            logger.error("---<JwtTokenProvider>---(validateToken)---Invalid JWT signature---")
        } catch (ex: MalformedJwtException) {
            logger.error("---<JwtTokenProvider>---(validateToken)---Invalid JWT token---")
        } catch (ex: ExpiredJwtException) {
            logger.error("---<JwtTokenProvider>---(validateToken)---Expired JWT token---")
        } catch (ex: UnsupportedJwtException) {
            logger.error("---<JwtTokenProvider>---(validateToken)---Unsupported JWT token---")
        } catch (ex: IllegalArgumentException) {
            logger.error("---<JwtTokenProvider>---(validateToken)---JWT claims string is empty---")
        }

        return false
    }
}