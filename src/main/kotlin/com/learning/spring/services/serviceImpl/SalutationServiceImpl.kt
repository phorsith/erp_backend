package com.learning.spring.services.serviceImpl

import com.learning.spring.models.Salutation
import com.learning.spring.repositories.SalutationRepository
import com.learning.spring.services.SalutationService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class SalutationServiceImpl: SalutationService {

    @Autowired
    lateinit var salutationRepository: SalutationRepository

    override fun findAllList(q: String?, page: Int, size: Int): Page<Salutation>? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findById(id: Long): Salutation? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun addNew(t: Salutation): Salutation? {
        return salutationRepository.save(t)
    }

    override fun updateObj(id: Long, t: Salutation): Salutation? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findAll(): List<Salutation>? {
        return salutationRepository.findAll()
    }
}