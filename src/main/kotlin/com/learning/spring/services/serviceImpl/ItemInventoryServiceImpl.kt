package com.learning.spring.services.serviceImpl

import com.learning.spring.models.ItemInventory
import com.learning.spring.repositories.ItemInventoryRepository
import com.learning.spring.services.ItemInventoryService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class ItemInventoryServiceImpl: ItemInventoryService {

    @Autowired
    lateinit var itemInventoryRepository: ItemInventoryRepository

    override fun findAllList(q: String?, page: Int, size: Int): Page<ItemInventory>? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findById(id: Long): ItemInventory? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun addNew(t: ItemInventory): ItemInventory? {
        return itemInventoryRepository.save(t)
    }

    override fun updateObj(id: Long, t: ItemInventory): ItemInventory? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findAll(): List<ItemInventory>? {
        return itemInventoryRepository.findAllByStatusTrueOrderByIdDesc()
    }
}