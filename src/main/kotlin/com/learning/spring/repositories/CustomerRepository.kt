package com.learning.spring.repositories

import com.learning.spring.models.Customer
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository

@Repository
interface CustomerRepository: JpaRepository<Customer,Long>,JpaSpecificationExecutor<Customer> {
    //fun findByIdAndStatusTrue(id: Long) : Users?
    fun findByIdAndStatusTrue(id: Long): Customer?
    fun findAllByStatusTrueOrderByIdDesc(): List<Customer>?
}