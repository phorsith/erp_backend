package com.learning.spring.services.serviceImpl

import com.learning.spring.models.ItemDropdown
import com.learning.spring.repositories.*
import com.learning.spring.services.ItemDropdownService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ItemDropdownServiceImpl: ItemDropdownService {
    @Autowired
    lateinit var
        itemGroupRepository: ItemGroupRepository
    @Autowired
    lateinit var
        itemBranchRepository: ItemBranchRepository
    @Autowired
    lateinit var
        supplierRepository: SupplierRepository
    @Autowired
    lateinit var
        valuateMethodRepository: ValuateMethodRepository
    @Autowired
    lateinit var uomRepository: UomRepository



    override fun getItemDropdownList(): ItemDropdown? {
        //val customerDropdown =CustomerDropdown()
        val itemDropdown = ItemDropdown()
          //  itemDropdown.itemPriceList = itemPriceRepository.findAllByStatusTrueOrderByIdDesc()
            itemDropdown.uomList = uomRepository.findAllByStatusTrueOrderByIdDesc()

            itemDropdown.itemGroupList = itemGroupRepository.findAllByStatusTrueOrderByIdDesc()
            itemDropdown.itemBranchList = itemBranchRepository.findAllByStatusTrueOrderByIdDesc()
            itemDropdown.supplierList = supplierRepository.findAllByStatusTrueOrderByIdDesc()
            itemDropdown.valuateMethodList = valuateMethodRepository.findAll()
        return itemDropdown
    }
}