package com.learning.spring.controller

import com.learning.spring.models.Gender
import com.learning.spring.services.GenderService
import com.learning.spring.utils.ResponseObjectMap
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/gender")
class GenderController {
    @Autowired
    lateinit var genderService: GenderService
    val response = ResponseObjectMap()

    @PostMapping()
    fun addGender(@RequestBody gender: Gender): MutableMap<String,Any> =
            response.responseObject(genderService.addNew(gender))

    @GetMapping("/all")
    fun getAllGender(): MutableMap<String,Any> = response.responseObject(genderService.findAll())

}