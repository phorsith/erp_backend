package com.learning.spring.models

import com.learning.spring.bases.model.BaseEntity
import javax.persistence.*


@Entity
@Table(name="payment_term")
class PaymentTerm(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long ? = null,
        @Column(name="payment_term_name")
        var paymentTermName: String ? = "",
        @Column(name="credit_days")
        var creditDays: Int ? = null,
        var description: String ? = ""
): BaseEntity()