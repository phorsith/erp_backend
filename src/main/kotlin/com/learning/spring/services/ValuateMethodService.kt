package com.learning.spring.services

import com.learning.spring.bases.service.BaseService
import com.learning.spring.models.ValuateMethod

interface ValuateMethodService: BaseService<ValuateMethod> {
}