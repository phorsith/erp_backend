package com.learning.spring.models


import com.fasterxml.jackson.annotation.JsonIgnore
import com.learning.spring.bases.model.BaseEntity
import javax.persistence.*

@Entity
@Table(name="item_variant_uom")
class ItemVariantUom(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long ? = null,
        @Column(name="conversion_factor")
        var conversionFactor: Int ? = null,

        @OneToOne
        @JoinColumn(name="uom_id", nullable = false)
        var uom: Uom? = null,
        @JsonIgnore
        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "item_id",nullable = false)
        var item: Item? = null
):BaseEntity()