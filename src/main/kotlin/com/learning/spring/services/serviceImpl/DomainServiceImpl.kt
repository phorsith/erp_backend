package com.learning.spring.services.serviceImpl

import com.learning.spring.models.Domain
import com.learning.spring.repositories.DomainRepository
import com.learning.spring.services.DomainService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Service
import javax.persistence.criteria.Predicate

@Service
class DomainServiceImpl: DomainService {
    @Autowired
    lateinit var domainRepository: DomainRepository

    override fun findAllList(q: String?, page: Int, size: Int): Page<Domain>? {
        return domainRepository.findAll({root, query, cb ->
            val predicates = ArrayList<Predicate>()
            if (q != null) {
                val name = cb.like(cb.upper(root.get("name")), "%${q.toUpperCase()}%")
                val email = cb.like(cb.upper(root.get("email")), "%${q.toUpperCase()}%")
                predicates.add(cb.or( name, email))
            }
            predicates.add(cb.equal(root.get<String>("status"), true))
            query.orderBy(cb.desc(root.get<String>("id")))
            cb.and(*predicates.toTypedArray())
        }, PageRequest.of(page, size))
    }

    override fun findById(id: Long): Domain? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun addNew(t: Domain): Domain? {
        return domainRepository.save(t)
    }

    override fun updateObj(id: Long, t: Domain): Domain? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findAll(): List<Domain>? {
        return domainRepository.findAllByStatusTrueOrderByIdDesc()
    }
}