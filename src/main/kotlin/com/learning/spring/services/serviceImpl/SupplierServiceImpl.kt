package com.learning.spring.services.serviceImpl

import com.learning.spring.models.Supplier
import com.learning.spring.repositories.SupplierRepository
import com.learning.spring.services.SupplierService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class SupplierServiceImpl: SupplierService {

    @Autowired
    lateinit var supplierRepository: SupplierRepository

    override fun findAllList(q: String?, page: Int, size: Int): Page<Supplier>? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findById(id: Long): Supplier? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun addNew(t: Supplier): Supplier? {
        return supplierRepository.save(t)
    }

    override fun updateObj(id: Long, t: Supplier): Supplier? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findAll(): List<Supplier>? {
        return supplierRepository.findAllByStatusTrueOrderByIdDesc()
    }
}