package com.learning.spring.repositories

import com.learning.spring.models.ValuateMethod
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository

@Repository
interface ValuateMethodRepository: JpaRepository<ValuateMethod,Long>,JpaSpecificationExecutor<ValuateMethod> {
}