package com.learning.spring.models

import com.learning.spring.bases.model.BaseEntity
import javax.persistence.*

@Entity
@Table(name="address")
class Address (
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long ? = null,
        var title: String? = "",
        @Column(name="address_line1")
        var addressLineOne: String? = "",
        @Column(name="address_line2")
        var addressLineTwo: String? = "",
        var state: String? = "",
        var country: String? = "",
        @Column(name="pastal_code")
        var pastalCode: String? = "",
        @Column(name="billing_address")
        var billingAddress: Boolean? = true,
        @Column(name="shipping_address")
        var shippingAddress: Boolean? = true

):BaseEntity()