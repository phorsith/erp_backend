package com.learning.spring.repositories

import com.learning.spring.models.Address
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository

@Repository
interface AddressRepository:JpaRepository<Address,Long>,JpaSpecificationExecutor<Address> {
    fun findAllByStatusTrueOrderByIdDesc(): List<Address>
}