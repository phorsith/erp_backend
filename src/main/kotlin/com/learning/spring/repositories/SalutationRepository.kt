package com.learning.spring.repositories

import com.learning.spring.models.Salutation
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository

@Repository
interface SalutationRepository: JpaRepository<Salutation,Any>,JpaSpecificationExecutor<Salutation> {
}