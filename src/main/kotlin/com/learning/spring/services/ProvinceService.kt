package com.learning.spring.services

import com.learning.spring.bases.service.BaseService
import com.learning.spring.models.Province

interface ProvinceService:BaseService<Province> {
}