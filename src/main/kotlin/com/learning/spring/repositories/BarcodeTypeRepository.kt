package com.learning.spring.repositories

import com.learning.spring.models.BarcodeType
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository

@Repository
interface BarcodeTypeRepository: JpaRepository<BarcodeType, Long>,JpaSpecificationExecutor<BarcodeType> {
    fun findAllByStatusTrueOrderByIdDesc(): List<BarcodeType>?
}