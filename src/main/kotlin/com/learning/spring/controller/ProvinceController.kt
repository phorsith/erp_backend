package com.learning.spring.controller

import com.learning.spring.models.Province
import com.learning.spring.services.ProvinceService
import com.learning.spring.utils.ResponseObjectMap
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/province")
class ProvinceController {
    @Autowired
    lateinit var provinceService: ProvinceService

    val response = ResponseObjectMap()

    @PostMapping()
    fun addProvince(@RequestBody province: Province): MutableMap<String,Any> =
        response.responseObject(provinceService.addNew(province))

    @GetMapping("/all")
    fun getAllProvince(): MutableMap<String,Any> = response.responseObject(provinceService.findAll())

}