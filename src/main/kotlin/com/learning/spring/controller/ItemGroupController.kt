package com.learning.spring.controller

import com.learning.spring.models.ItemGroup
import com.learning.spring.services.ItemGroupService
import com.learning.spring.utils.ResponseObjectMap
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/item_group")
class ItemGroupController {
    @Autowired
    lateinit var itemGroupService: ItemGroupService
    val response = ResponseObjectMap()

    @PostMapping()
    fun addItemGroup(@RequestBody itemGroup: ItemGroup): MutableMap<String,Any>
            = response.responseObject(itemGroupService.addNew(itemGroup))

    @GetMapping("/all")
    fun getAllItemGroup(): MutableMap<String,Any> = response.responseObject(itemGroupService.findAll())


}