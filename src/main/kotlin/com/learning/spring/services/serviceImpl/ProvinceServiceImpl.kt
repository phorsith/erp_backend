package com.learning.spring.services.serviceImpl

import com.learning.spring.models.Province
import com.learning.spring.repositories.ProvinceRepository
import com.learning.spring.services.ProvinceService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class ProvinceServiceImpl:ProvinceService {

    @Autowired
    lateinit var provinceRepository: ProvinceRepository

    override fun findAllList(q: String?, page: Int, size: Int): Page<Province>? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findById(id: Long): Province? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun addNew(t: Province): Province? {
        return provinceRepository.save(t)
    }

    override fun updateObj(id: Long, t: Province): Province? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findAll(): List<Province>? {
        return provinceRepository.findAll()
    }
}