package com.learning.spring.repositories

import com.learning.spring.models.CustomerGroup
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository

@Repository
interface CustomerGroupRepository: JpaRepository<CustomerGroup,Long>,JpaSpecificationExecutor<CustomerGroup>{
}