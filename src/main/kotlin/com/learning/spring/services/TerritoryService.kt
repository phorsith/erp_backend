package com.learning.spring.services

import com.learning.spring.bases.service.BaseService
import com.learning.spring.models.Territory

interface TerritoryService:BaseService<Territory> {
}