package com.learning.spring.repositories

import com.learning.spring.models.Domain
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository

@Repository
interface DomainRepository:JpaRepository<Domain, Long>,JpaSpecificationExecutor<Domain> {
    fun findAllByStatusTrueOrderByIdDesc(): List<Domain>?
}