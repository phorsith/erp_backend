package com.learning.spring.services.serviceImpl

import com.learning.spring.models.Token
import com.learning.spring.repositories.TokenRepository
import com.learning.spring.services.TokenService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class TokenServiceImpl : TokenService{

    @Autowired
    lateinit var tokenRepository: TokenRepository

    override fun findByToken(accessToken: Token): Token? {
        return tokenRepository.findByToken(accessToken.token!!)
    }

    @Transactional
    override fun deleteByToken(token: String): Int {
        return tokenRepository.deleteByToken(token)
    }

    override fun findAllList(q: String?, page: Int, size: Int): Page<Token>? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findById(id: Long): Token? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun addNew(t: Token): Token? {
        return tokenRepository.save(t)
    }

    override fun updateObj(id: Long, t: Token): Token? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findAll(): List<Token>? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}