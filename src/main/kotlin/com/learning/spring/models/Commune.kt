package com.learning.spring.models

import com.fasterxml.jackson.annotation.JsonIgnore
import com.learning.spring.bases.model.BaseEntity
import javax.persistence.*

@Entity
@Table(name="commune")
class Commune(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long ? = null,
        var commune: String?="",
        @Column(name = "commune_kh")
        var communeKhmer: String?="",
        var commid: String? = "",
        @ManyToOne
        @JoinColumn(name="district_id", nullable=false)
        var district: District ?= null,
        @JsonIgnore
        @OneToMany(mappedBy = "commune", cascade = [CascadeType.ALL])
        var village: MutableList<Village> ?= null

): BaseEntity()