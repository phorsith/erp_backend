package com.learning.spring.services.serviceImpl

import com.learning.spring.models.ValuateMethod
import com.learning.spring.repositories.ValuateMethodRepository
import com.learning.spring.services.ValuateMethodService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class ValuateMethodServiceImpl: ValuateMethodService {

    @Autowired
    lateinit var valuateMethodRepository: ValuateMethodRepository

    override fun findAllList(q: String?, page: Int, size: Int): Page<ValuateMethod>? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findById(id: Long): ValuateMethod? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun addNew(t: ValuateMethod): ValuateMethod? {
        return valuateMethodRepository.save(t)
    }

    override fun updateObj(id: Long, t: ValuateMethod): ValuateMethod? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findAll(): List<ValuateMethod>? {
        return valuateMethodRepository.findAll()
    }
}