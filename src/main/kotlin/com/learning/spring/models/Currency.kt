package com.learning.spring.models

import com.learning.spring.bases.model.BaseEntity
import javax.persistence.*

@Entity
@Table(name="currency")
class Currency(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long ? = null,
        @Column(name="currency_name")
        var currencyName: String ? = ""
):BaseEntity()