package com.learning.spring.controller

import com.learning.spring.models.Salutation
import com.learning.spring.services.SalutationService
import com.learning.spring.utils.ResponseObjectMap
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/salutation")
class SalutationController {
    @Autowired
    lateinit var salutationService: SalutationService

    val response = ResponseObjectMap()

    @PostMapping()
    fun addNew(@RequestBody salutation: Salutation): MutableMap<String,Any>
            = response.responseObject(salutationService.addNew(salutation))

    @GetMapping("/all")
    fun getAllSalutation(): MutableMap<String,Any> = response.responseObject(salutationService.findAll())
}