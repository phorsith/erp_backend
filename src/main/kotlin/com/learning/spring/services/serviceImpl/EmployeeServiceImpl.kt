package com.learning.spring.services.serviceImpl

import com.learning.spring.models.Employee
import com.learning.spring.repositories.EmployeeRepository
import com.learning.spring.services.EmployeeService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class EmployeeServiceImpl: EmployeeService {
    @Autowired
    lateinit var employeeRepository: EmployeeRepository

    override fun findAllList(q: String?, page: Int, size: Int): Page<Employee>? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findById(id: Long): Employee? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun addNew(t: Employee): Employee? {
       return employeeRepository.save(t)
    }

    override fun updateObj(id: Long, t: Employee): Employee? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findAll(): List<Employee>? {
        return employeeRepository.findAllByStatusTrueOrderByIdDesc()
    }
}