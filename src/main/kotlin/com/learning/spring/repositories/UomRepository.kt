package com.learning.spring.repositories

import com.learning.spring.models.Uom
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository

@Repository
interface UomRepository: JpaRepository<Uom,Long>,JpaSpecificationExecutor<Uom> {
    fun findAllByStatusTrueOrderByIdDesc(): List<Uom>?
}