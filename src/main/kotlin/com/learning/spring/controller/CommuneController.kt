package com.learning.spring.controller

import com.learning.spring.models.Commune
import com.learning.spring.services.CommuneService
import com.learning.spring.utils.ResponseObjectMap
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/commune")
class CommuneController {

    @Autowired
    lateinit var communeService: CommuneService

    var response = ResponseObjectMap()
    @PostMapping()
    fun addCommune(@RequestBody commune: Commune): MutableMap<String,Any> =
        response.responseObject(communeService.addNew(commune))

    @GetMapping("/all")
    fun getAllCommune(): MutableMap<String,Any> =
        response.responseObject(communeService.findAll())

    @GetMapping("/district/{distId}")
    fun getCommuneByDistrictId(@PathVariable(value ="distId") distId: Long):MutableMap<String,Any> =
        response.responseObject(communeService.getCommuneByDistrictId(distId))
}