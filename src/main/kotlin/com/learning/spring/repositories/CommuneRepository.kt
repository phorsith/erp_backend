package com.learning.spring.repositories

import com.learning.spring.models.Commune
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository

@Repository
interface CommuneRepository: JpaRepository<Commune,Long>,JpaSpecificationExecutor<Commune> {
    fun findByDistrictIdAndStatusTrue(distId: Long): List<Commune>
}