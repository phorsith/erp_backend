package com.learning.spring.controller

import com.learning.spring.models.ItemAttribute
import com.learning.spring.services.ItemAttributeService
import com.learning.spring.utils.ResponseObjectMap
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/item-attribute")
class ItemAttributeController {
    @Autowired
    lateinit var itemAttributeService: ItemAttributeService
    val response = ResponseObjectMap()

    @PostMapping()
    fun addItem(@RequestBody itemAttribute: ItemAttribute): MutableMap<String,Any>
        = response.responseObject(itemAttributeService.addNew(itemAttribute))

    @GetMapping("/all")
    fun getAllItem(): MutableMap<String,Any> = response.responseObject(itemAttributeService.findAll())


}