package com.learning.spring.services.serviceImpl

import com.learning.spring.models.CustomerGroup
import com.learning.spring.repositories.CustomerGroupRepository
import com.learning.spring.services.CustomerGroupService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class CustomerGroupServiceImpl:CustomerGroupService {

    @Autowired
    lateinit var customerGroupRepository: CustomerGroupRepository

    override fun findAllList(q: String?, page: Int, size: Int): Page<CustomerGroup>? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findById(id: Long): CustomerGroup? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun addNew(t: CustomerGroup): CustomerGroup? {
        return customerGroupRepository.save(t)

    }

    override fun updateObj(id: Long, t: CustomerGroup): CustomerGroup? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findAll(): List<CustomerGroup>? {
       return customerGroupRepository.findAll()
    }
}