package com.learning.spring.services.serviceImpl

import com.learning.spring.models.Barcode
import com.learning.spring.repositories.BarcodeRepository
import com.learning.spring.services.BarcodeService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class BarcodeServiceImpl: BarcodeService {

    @Autowired
    lateinit var barcodeRepository: BarcodeRepository

    override fun findAllList(q: String?, page: Int, size: Int): Page<Barcode>? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findById(id: Long): Barcode? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun addNew(t: Barcode): Barcode? {
        return barcodeRepository.save(t)
    }

    override fun updateObj(id: Long, t: Barcode): Barcode? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findAll(): List<Barcode>? {
       return barcodeRepository.findAllByStatusTrueOrderByIdDesc()
    }
}