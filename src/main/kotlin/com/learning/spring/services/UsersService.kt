package com.learning.spring.services

import com.learning.spring.bases.service.BaseService
import com.learning.spring.models.UserRole
import com.learning.spring.models.Users
import org.springframework.web.multipart.MultipartFile

interface UsersService : BaseService<Users>{
    fun uploadUserProfile(id: Long, image: MultipartFile): Users?
    fun applyRolesToUser(userId: Long, rolesList: LongArray): List<UserRole>
}