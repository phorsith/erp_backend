package com.learning.spring.repositories

import com.learning.spring.models.Supplier
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository

@Repository
interface SupplierRepository: JpaRepository<Supplier,Long>,JpaSpecificationExecutor<Supplier> {
    fun findAllByStatusTrueOrderByIdDesc(): List<Supplier>?
}