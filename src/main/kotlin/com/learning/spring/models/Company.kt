package com.learning.spring.models

import com.learning.spring.bases.model.BaseEntity
import javax.persistence.*

@Entity
@Table(name="company")
class Company(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long ? = null,
        @Column(name="company_name")
        var companyName: String ? = "",
        var abbr: String? = "",
        @Column(name="company_logo")
        var companyLogo: String? = "",
        var description: String? = "",
        @Column(name="monthly_sale_target")
        var monthlySaleTarget: String? = "",
        var email: String? = "",
        @Column(name="working_our")
        var workingOur: String? = "",
        var phone: String? = ""
):BaseEntity()