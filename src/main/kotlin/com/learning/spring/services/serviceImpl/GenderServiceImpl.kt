package com.learning.spring.services.serviceImpl

import com.learning.spring.models.Gender
import com.learning.spring.repositories.GenderRepository
import com.learning.spring.services.GenderService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class GenderServiceImpl:GenderService {

    @Autowired
    lateinit var genderRepository: GenderRepository

    override fun findAllList(q: String?, page: Int, size: Int): Page<Gender>? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findById(id: Long): Gender? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun addNew(t: Gender): Gender? {
        return genderRepository.save(t)
    }

    override fun updateObj(id: Long, t: Gender): Gender? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findAll(): List<Gender>? {
        return genderRepository.findAll()
    }
}