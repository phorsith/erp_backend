package com.learning.spring.controller

import com.learning.spring.models.AccountType
import com.learning.spring.services.AccountTypeService
import com.learning.spring.utils.ResponseObjectMap
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/account-type")
class AccountTypeController {
    @Autowired
    lateinit var accountTypeService: AccountTypeService
    val response = ResponseObjectMap()

    @PostMapping()
    fun addAccountType(@RequestBody accountType: AccountType): MutableMap<String,Any> =
        response.responseObject(accountTypeService.addNew(accountType))

    @GetMapping("/all")
    fun getAllAccountType(): MutableMap<String,Any> =
        response.responseObject(accountTypeService.findAll())

}