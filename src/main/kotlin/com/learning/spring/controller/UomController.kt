package com.learning.spring.controller


import com.learning.spring.models.Uom
import com.learning.spring.services.UomService
import com.learning.spring.utils.ResponseObjectMap
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/uom")
class UomController {
    @Autowired
    lateinit var uomService: UomService
    val response = ResponseObjectMap()

    @PostMapping()
    fun addItem(@RequestBody uom: Uom): MutableMap<String,Any>
            = response.responseObject(uomService.addNew(uom))

    @GetMapping("/all")
    fun getAllItem(): MutableMap<String,Any> = response.responseObject(uomService.findAll())


}