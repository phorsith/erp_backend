package com.learning.spring.repositories

import com.learning.spring.models.UserRole
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import javax.transaction.Transactional

interface UsersRoleRepository: JpaRepository<UserRole,Long>,JpaSpecificationExecutor<UserRole> {

    @Query(value = "UPDATE user_role SET status = false WHERE user_id = ?1", nativeQuery = true)
    @Modifying
    @Transactional
    fun deleteUserRoleByUserId(userId: Long)
}