package com.learning.spring.repositories

import com.learning.spring.models.DeliveryWarehouse
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository

@Repository
interface DeliveryWarehouseRepository:JpaRepository<DeliveryWarehouse,Long>,JpaSpecificationExecutor<DeliveryWarehouse> {

}