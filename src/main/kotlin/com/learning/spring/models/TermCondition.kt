package com.learning.spring.models

import com.learning.spring.bases.model.BaseEntity
import javax.persistence.*

@Entity
@Table(name="term_condition")
class TermCondition (
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long ? = null,
        var title: String?="",
        var description: String? = "",
        var disable: Boolean? = true
): BaseEntity()