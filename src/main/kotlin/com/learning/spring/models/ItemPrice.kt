package com.learning.spring.models

import com.learning.spring.bases.model.BaseEntity
import javax.persistence.*

@Entity
@Table(name="item_price")
class ItemPrice(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long ? = null,
        @Column(name="price_name")
        var priceName: String? = "",
        var note: String? = "",
        @Column(name="minimum_qty")
        var minimumQty: Int? = null,
        var rate: Int? = null,
        @Column(name="standar_selling")
        var standardSelling: Int? = null,
        @Column(name="standar_buying")
        var standardBuying: Int? = null,
        var buying: Boolean ?= true,
        var selling: Boolean ?= true,
        var enable: Boolean ?= true
): BaseEntity()