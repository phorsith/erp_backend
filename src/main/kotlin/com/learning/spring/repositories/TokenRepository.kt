package com.learning.spring.repositories

import com.learning.spring.models.Token
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository

@Repository
interface TokenRepository: JpaRepository<Token,Long>,JpaSpecificationExecutor<Token> {
    fun findByToken(token : String) : Token?
    fun deleteByToken(token : String) : Int
}