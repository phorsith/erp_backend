package com.learning.spring.controller

import com.learning.spring.models.Village
import com.learning.spring.services.VillageService
import com.learning.spring.utils.ResponseObjectMap
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/village")
class VillageController {

    @Autowired
    lateinit var villageService: VillageService

    val response = ResponseObjectMap()

    @PostMapping()
    fun addVillage(@RequestBody village: Village): MutableMap<String,Any> =
        response.responseObject(villageService.addNew(village))

    @GetMapping("/all")
    fun getAllVillage(): MutableMap<String,Any> =
        response.responseObject(villageService.findAll())

    @GetMapping("/commune/{commId}")
        fun getVillageByCommuneId(@PathVariable(value ="commId") commId: Long):MutableMap<String,Any> =
            response.responseObject(villageService.getVillageByCommuneId(commId))
}