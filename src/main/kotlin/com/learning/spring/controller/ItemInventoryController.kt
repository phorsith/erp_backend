package com.learning.spring.controller

import com.learning.spring.models.ItemInventory
import com.learning.spring.services.ItemInventoryService
import com.learning.spring.utils.ResponseObjectMap
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/item-inventory")
class ItemInventoryController {
    @Autowired
    lateinit var itemInventoryService: ItemInventoryService

    val response = ResponseObjectMap()

    @PostMapping()
    fun addItemInventory(@RequestBody  itemInventory: ItemInventory): MutableMap<String,Any>
        = response.responseObject(itemInventoryService.addNew(itemInventory))

    @GetMapping("/all")
    fun getAllItemInventory(): MutableMap<String,Any> =
        response.responseObject(itemInventoryService.findAll())

}