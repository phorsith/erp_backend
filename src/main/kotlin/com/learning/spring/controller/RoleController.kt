package com.learning.spring.controller

import com.learning.spring.models.Role
import com.learning.spring.services.RoleService
import com.learning.spring.utils.ResponseObjectMap
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/role")
class RoleController {
    @Autowired
    lateinit var roleService: RoleService

    val response = ResponseObjectMap()

    @PostMapping()
    fun addNewRole(@RequestBody role: Role): MutableMap<String,Any> = response.responseObject(roleService.addNew(role))

    @GetMapping("/all")
    fun getAllRole():MutableMap<String,Any> = response.responseObject(roleService.findAll())

}