package com.learning.spring.services.serviceImpl

import com.learning.spring.exceptions.ResourceNotFoundException
import com.learning.spring.models.Role
import com.learning.spring.models.UserRole
import com.learning.spring.models.Users
import com.learning.spring.repositories.RoleRepository
import com.learning.spring.repositories.UsersRepository
import com.learning.spring.repositories.UsersRoleRepository
import com.learning.spring.services.UsersService
import com.learning.spring.utils.FileUpload
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import javax.persistence.criteria.Predicate

@Suppress("UNREACHABLE_CODE")
@Service
class UsersServiceImpl : UsersService {

    @Autowired
    lateinit var usersRepository: UsersRepository

    @Autowired
    lateinit var usersRoleRepository: UsersRoleRepository

    @Autowired
    lateinit var roleRepository: RoleRepository

    var bCryptPasswordEncoder = BCryptPasswordEncoder()

    @Value("\${users.image}")
    var userImage: String? = null

    override fun uploadUserProfile(id: Long, image: MultipartFile): Users? {

        val originalFileName = FileUpload.storeImage(image, userImage)
        val users = usersRepository.findByIdAndStatusTrue(id)
        users!!.imagePart = "/user/images/$originalFileName"
        println("user part ${users.imagePart}")
        users.image = originalFileName
        println("user part ${users.image}")

        return usersRepository.save(users)
    }

    @Throws(ResourceNotFoundException::class)
    override fun applyRolesToUser(userId: Long, rolesList: LongArray): List<UserRole> {
        if(!usersRepository.existsById(userId)) throw throw ResourceNotFoundException("User not exit by id : $userId")
        usersRoleRepository.deleteUserRoleByUserId(userId)

        val userRolesList = ArrayList<UserRole>()
        for (role in rolesList){
            if(!roleRepository.existsById(role))continue
            val userRole = UserRole()
            userRole.user = Users(userId)
            println("### userRole user ${ userRole.user } ")

            userRole.role = Role(role)
            println("### userRole role ${ userRole.role } ")

            userRolesList.add(userRole)
            println("### userRolesList $userRolesList ")

            userRole.status = true
        }
        return usersRoleRepository.saveAll(userRolesList)
    }

    override fun findAllList(q: String?, page: Int, size: Int): Page<Users>? {

         return usersRepository.findAll({root, query, cb ->
                    val predicates = ArrayList<Predicate>()
                    if (q != null) {
                        val name = cb.like(cb.upper(root.get("name")), "%${q.toUpperCase()}%")
                        val email = cb.like(cb.upper(root.get("email")), "%${q.toUpperCase()}%")
                        predicates.add(cb.or( name, email))
                    }
                    predicates.add(cb.equal(root.get<String>("status"), true))
                    query.orderBy(cb.desc(root.get<String>("id")))
                    cb.and(*predicates.toTypedArray())
                }, PageRequest.of(page, size))
    }

    override fun findById(id: Long): Users? {
       checkUsersException(id)
        return usersRepository.findByIdAndStatusTrue(id)
    }

    override fun addNew(t: Users): Users? {
        val user = t
        t.fullName = t.lastName +" "+t.firstName
        return usersRepository.save(user)
    }

    override fun updateObj(id: Long, t: Users): Users? {
        val user = usersRepository.findByIdAndStatusTrue(id)
        checkUsersException(id)
        if(!t.status) user?.status = false
        val password : String? = t.password
            if (!password.isNullOrEmpty()) {
                val hasPass = bCryptPasswordEncoder.encode(password)
                user?.password = hasPass
            }
            user?.firstName = t.firstName
            user?.lastName = t.lastName
            user?.fullName = t.lastName +" "+t.firstName
            user?.userName = t.userName
            user?.email = t.email
            user?.gender = t.gender
            user?.phoneNo = t.phoneNo
            user?.password = password

        return usersRepository.save(user!!)
    }

    override fun findAll(): List<Users>? {
        return usersRepository.findAllByStatusTrueOrderByIdDesc()
    }

    fun checkUsersException(id: Long){
        if(usersRepository.findByIdAndStatusTrue(id) == null) throw ResourceNotFoundException("Users ID : $id doestn't Exits!!!")
    }

}