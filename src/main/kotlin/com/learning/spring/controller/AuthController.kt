package com.learning.spring.controller


import com.learning.spring.models.LoginRequest
import com.learning.spring.models.Token
import com.learning.spring.models.response.ApiResponse
import com.learning.spring.security.JwtTokenProvider
import com.learning.spring.services.TokenService
import com.learning.spring.services.UsersService
import com.learning.spring.utils.ResponseObjectMap
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.bind.annotation.*

@Suppress("NAME_SHADOWING")
@RestController
@RequestMapping("api/auth")
class AuthController {


    @Autowired
    lateinit var tokenService: TokenService

    @Autowired
    lateinit var tokenProvider: JwtTokenProvider

    @Autowired
    lateinit var authenticationManager: AuthenticationManager

    @Autowired
    lateinit var userService: UsersService
    var response = ResponseObjectMap()

    @PostMapping("/login")
    fun authenticateUser(@RequestBody loginReq: LoginRequest) : ResponseEntity<Any>?{

        val authentication = authenticationManager.authenticate(
                UsernamePasswordAuthenticationToken(loginReq.userName,loginReq.password)
        )
        SecurityContextHolder.getContext().authentication = authentication
        println("===========================")
        val accessToken = tokenProvider.generateToken(authentication)
        println("token: ${accessToken.token}")
        return ApiResponse().response("accessToken,expireIn,tokenType",accessToken.token!!,accessToken.expireIn!!,"Bearer")
    }


    @PostMapping("/refresh-token")
    fun refreshToken(@RequestBody accessToken: Token) : ResponseEntity<Any>?{
        val isValid = tokenProvider.validateToken(accessToken.token)
        if(!isValid)
            return ApiResponse().response(401)

        if(tokenService.deleteByToken(accessToken.token!!) <1) return ApiResponse().response(401)

        val accessToken = tokenProvider.refreshToken(accessToken.token!!)
        val userId = tokenProvider.getUserIdFromJWT(accessToken.token)

        userService.findById(userId) ?: return ApiResponse().response(401)

        tokenService.addNew(accessToken)
        return ApiResponse().response("accessToken,expireIn,tokenType",accessToken.token!!,accessToken.expireIn!!,"Bearer")
    }

}