package com.learning.spring.models

import com.learning.spring.bases.model.BaseEntity
import javax.persistence.*

@Entity
@Table(name="customer")
class Customer(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long ? = null,
        var name: String ? = "",
        @Column(name = "name_kh")
        var nameKhmer: String ? = "",
        @Column(name = "phone_no")
        var phoneNumber: String? = "",
        @Column(name = "house_no")
        var houseNumber: Int? = null,
        var street: String? = "",
        var address: String? = "",
        @Column(name = "billing_address")
        var villingAddress: String? = "",
        @Column(name = "shipping_address")
        var shippingAddress: String? = "",
        var disable: Boolean? = false,
        @Column(name = "internal_customer")
        var internalCustomer: Boolean? = false,
        @Column(name = "credit_limit_check")
        var creditLimitCheck: Boolean? = false,
        @OneToOne
        @JoinColumn(name = "cusgroup_id", nullable = false)
        var customerGroup: CustomerGroup? = null,
        @OneToOne
        @JoinColumn(name = "gender_id", nullable = false)
        var gender: Gender ?= null,
        @OneToOne
        @JoinColumn(name = "salutation_id", nullable = false)
        var salutation: Salutation? = null,
        @OneToOne
        @JoinColumn(name = "custype_id", nullable = false)
        var customerType: CustomerType? = null,
        @OneToOne
        @JoinColumn(name = "currency_id", nullable = false)
        var currency: Currency? = null,
        @OneToOne
        @JoinColumn(name = "payentterm_id", nullable = false)
        var paymentTerm: PaymentTerm? = null,
        @OneToOne
        @JoinColumn(name = "price_id", nullable = false)
        var price: Price? = null,
        @OneToOne
        @JoinColumn(name = "delivery_id", nullable = false)
        var deliveryWarehouse: DeliveryWarehouse? = null,
        @OneToOne
        @JoinColumn(name = "territory_id", nullable = false)
        var territory: Territory? = null,
        @OneToOne
        @JoinColumn(name = "sale_id", nullable = false)
        var salePerson: SalePerson? = null,
        @OneToOne
        @JoinColumn(name = "province_id", nullable = false)
        var province: Province? = null,
        @OneToOne
        @JoinColumn(name = "district_id", nullable = false)
        var district: District? = null,
        @OneToOne
        @JoinColumn(name = "commune_id", nullable = false)
        var commune: Commune? = null,
        @OneToOne
        @JoinColumn(name = "village_id", nullable = false)
        var village: Village? = null


):BaseEntity()